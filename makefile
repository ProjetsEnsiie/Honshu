SRC = src/
TEST = test/
BIN = bin/
BUILD = build/
CC = gcc -Wall -Wextra -ansi
CCUNIT = -lcunit

all : $(BIN)tuile $(BIN)SDL $(TEST)testTuile doxy clean

$(BIN)tuile : $(BUILD)Plateau.o $(BUILD)tuile.o $(BUILD)insertion.o $(BUILD)Liste_tuiles.o $(BUILD)pile.o $(BUILD)loadAndSave.o $(BUILD)jeu.o $(BUILD)compte_point.o $(BUILD)solveur_naif.o $(BUILD)solveur.o $(BUILD)solveur_bis.o $(BUILD)genetique.o $(BUILD)solveurGenetique.o $(BUILD)main.o
	$(CC) $^ -o $(BIN)tuile -lm

$(BIN)SDL : $(BUILD)Plateau.o $(BUILD)tuile.o $(BUILD)insertion.o $(BUILD)Liste_tuiles.o $(BUILD)liste_tuile_surface.o $(BUILD)loadAndSave.o $(BUILD)pile.o $(BUILD)jeu.o $(BUILD)compte_point.o $(BUILD)solveur_naif.o $(BUILD)SDL_image.o
	$(CC) $^ -o $(BIN)SDL -lSDLmain -lSDL -lSDL_ttf -lm

$(BUILD)SDL_image.o : $(SRC)SDL_image.c $(SRC)SDL_image.h
	gcc -lm -c $< -o $@ -lm

$(BUILD)%.o : $(SRC)%.c $(SRC)%.h
	$(CC) -lm -c $< -o $@ -lm

$(TEST)testTuile : $(BUILD)Plateau.o $(BUILD)tuile.o $(BUILD)insertion.o $(BUILD)Liste_tuiles.o $(BUILD)loadAndSave.o $(BUILD)pile.o $(BUILD)jeu.o $(BUILD)solveur.o $(BUILD)compte_point.o $(BUILD)genetique.o $(BUILD)solveurGenetique.o $(TEST)tuileTests.c
	$(CC) $^ -o $(BIN)testTuile $(CCUNIT) -lm



doxy :
	doxygen Doxyfile

clean:
	rm $(BUILD)*.o
