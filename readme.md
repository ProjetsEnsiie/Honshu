# Projet Honshu

## Etapes d'installation :

### Intaller l'environnemnet de developpement pour le C :

```
$ sudo apt-get install build-essential

```
### Intaller CUnit, Doxyfile, SDL, et la police ttf :

```
$ sudo apt-get install libcunit1 libcunit1-doc libcunit1-dev
$ sudo apt-get install Doxyfile
$ sudo apt-get install libsdl1.2-dev
$ sudo apt-get install libsdl-ttf2.0-dev
```

### Récupération du projet :

```
$ git clone https://gitlab.com/ProjetsEnsiie/Honshu.git
```

### Compilation et execution du programme:

```
$ make (pour compiler tout le projet)
$ ./bin/tuile (pour le jeu console)
$ ./bin/SDL (pour le jeu graphique)
$ ./bin/testTuile (pour les tests)
```

### Liens utiles
* http://www.ensiie.fr/~dimitri.watel/teaching/s2_project/Courses/projet_info.pdf
* http://skutnik.iiens.net/cours/IPI/
* https://sites.uclouvain.be/SystInfo/notes/Outils/html/cunit.html
* http://nvie.com/posts/a-successful-git-branching-model/
