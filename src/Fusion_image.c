#include <stdlib.h>
#include <SDL/SDL.h>


Uint32 GetPixel(SDL_Surface *screen, int x, int y)
{
    return *(Uint32 *) ((Uint8 *)screen->pixels + y * screen->pitch + x * screen->format->BytesPerPixel);
}

void SetPixel(SDL_Surface *screen, int x, int y, Uint32 pixel)
{
    *(Uint32 *) ((Uint8 *)screen->pixels + y * screen->pitch + x * screen->format->BytesPerPixel) = pixel;
}

int main ( int argc, char** argv )
{
    SDL_Surface *layer1 = NULL, *layer2 = NULL, *result;
    unsigned i, j;
    Uint32 pix1, pix2;
    Uint8 r1, r2, g1, g2, b1, b2;

    if ( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
        fprintf( stderr, "Unable to init SDL: %s\n", SDL_GetError() );
        return 1;
    }

    layer1 = SDL_LoadBMP("/../img/Officiel/Foret.bmp" );  // CT
    layer2 = SDL_LoadBMP("/../img/Officiel/Lac.bmp" );  // PET

    if (layer1 && layer2)
    {
        result = SDL_CreateRGBSurface(SDL_SWSURFACE, layer1->w, layer1->h, 32, 0, 0, 0, 0);

        for  (i=0; i<layer1->w; i++)
            for (j=0; j<layer1->h; j++)
            {
                pix1 = GetPixel(layer1, i, j);
                pix2 = GetPixel(layer2, i, j);

                SDL_GetRGB(pix1, layer1->format, &r1, &g1, &b1);
                SDL_GetRGB(pix2, layer2->format, &r2, &g2, &b2);

                SetPixel(result, i, j, SDL_MapRGB( result->format, r1, g1, b1));
            }

        SDL_SaveBMP(result, "result.bmp" );
    }

    SDL_Quit();
    return 0;
}
