#include "Plateau.h"
#include "tuile.h"
#include "insertion.h"

#include <stdio.h>
#include <stdlib.h>

#include "Liste_tuiles.h"


typedef cell* list;

list create_list()
{
    return NULL;
}

int list_is_empty(list l)
{
    return (l==NULL);
}

list list_vADD(list l, plateau x)
{
    list newcell = (list)malloc(sizeof(cell));
    newcell->value=x;
    newcell->next=l;
    return newcell;
}

void list_eADD(list* pl, plateau x)
{
    *pl = list_vADD(*pl, x);
}

void print_list (list l)
{
    int tuiles_largeur = 4;
    int i=0;
    list currentlist=l;
    plateau* plats = malloc(sizeof(plateau)*tuiles_largeur);
    printf("\n");

    while (currentlist!=NULL)
    {
      plats[i%tuiles_largeur]=currentlist->value;
      if (i%tuiles_largeur+1==tuiles_largeur || currentlist->next == NULL){
        print_plusieurs_plateaux(plats, (i%tuiles_largeur)+1);
        printf("\n");
      }
      currentlist=currentlist->next;
      i++;
    }
}

int longer_list(list l)
{
    int i=0;
    list currentlist=l;
    while (currentlist!=NULL)
    {
        i++;
        currentlist=currentlist->next;
    }
    return i;
}

void list_reverse(list* pl)
{
    list previous;
    list current;
    list next;
    current=*pl;
    previous=NULL;
    while (current!=NULL)
    {
        next=current->next;
        current->next=previous;
        previous=current;
        current=next;
    }
    *pl=previous;
}

int list_elementAt(list l, int index, plateau* ret)
{
    list current=l;
    while (current!=NULL && index>0)
    {
        index--;
        current=current->next;
    }
    if (current==NULL)
    {
        return 0;
    }
    else
    {
        *ret=current->value;
        return 1;
    }
}

int list_element_id(list l, int id, plateau* ret)
{
    list current=l;
    while (current!=NULL)
    {
        if (current==NULL)
        {
            return -1;
        }
        else if ((current->value.values[0][0].numero) == id)
        {
            *ret=current->value;
            return 1;
        }
        else
        {
            current=current->next;
        }
    }
    return -1;
}

int list_element_idbis(list l, int id, plateau** ret)
{
    list current=l;
    while (current!=NULL)
    {
        if (current==NULL)
        {
            return -1;
        }
        else if ((current->value.values[0][0].numero) == id)
        {
            *ret=&(current->value);
            return 1;
        }
        else
        {
            current=current->next;
        }
    }
    return -1;
}


int rm_element(list* pl, int idx)
{
    if(idx==0)
    {
       *pl = (*pl)->next;
        return 1;
    }
    list final = *pl;
    list previous;
    list current=final;
    while (current!=NULL && idx>0)
    {
        idx--;
        previous=current;
        current=current->next;
    }
    if (current==NULL)
    {
        return 0;
    }
    else
    {
        previous->next=(current->next);
        free_plateau(current->value);
        free(current);
        return 1;
    }
}

void free_liste(list l){
    while (!list_is_empty(l)){
        free_plateau(l->value);
        list tmp = l;
        l = l->next;
        free(tmp);
    }
    free(l);
}

list copie_list(list l){

    if(l == NULL)
    {
        return create_list();
    }
    list cl = create_list() ;
    list cur = l;

    while(!list_is_empty(cur)){
        list_eADD(&cl,copie_plateau(cur->value));
        cur = cur->next;
    }

    return cl;
}


/*int rm_element(list* pl, int idx)
{
    list current=*pl;
	list prec;
    while (current!=NULL && idx>0)
    {
        idx--;
		prec = current;
        current=current->next;
    }
    if (current==NULL)
    {
        return 0;
    }
    else
    {
        prec = current->next;
		free_plateau(current->value);
		free(current);
        return 1;
    }
}*/
