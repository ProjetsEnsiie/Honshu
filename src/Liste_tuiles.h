/**
*\file Liste_tuiles.h
*Ce fichier decrit les fonctions concernant les listes de tuile. Il contient 13 fonctions :
*- create_list() renvoie une liste vide
*- list_is_empty(l) renvoie 0 si la est liste vide 1 sinon
*- list_vADD( l, x) renvoie une liste avec un plateau en plus
*- list_eADD(pl, x) renvoie une liste avec un plateau en plus par effet
*- print_list ( l) affiche une liste
*- longer_list(l) renvoie la taile d'une liste
*- list_reverse(pl) Renverse l'ordre des éléments d'une liste
*- list_elementAt(l,index,ret) Récupère une tuiile d'une liste en fonction de sa position
*- list_element_id(l, id, ret) Récupère une tuiile d'une liste en fonction de son numéro
*- list_element_idbis(l, id, ret) Récupère une tuiile d'une liste en fonction de son numéro (comme list_element_id mais sans copie)
*- rm_element( pl, idx) efface un element de la liste
*/

#include "Plateau.h"
#include "tuile.h"
#include "insertion.h"
#include <stdio.h>

#ifndef __LISTE_TUILE_h__
#define __LISTE_TUILE_h__


typedef struct cell cell;
struct cell
{
    plateau value;
    cell* next;
};

typedef cell* list;

/**
* \brief renvoie une liste vide
* \return la liste associee
*/
list create_list();

/**
* \brief renvoie 0 si la est liste vide 1 sinon
* \param l Une liste
* \return 0 ou 1
*/
int list_is_empty(list l);

/**
* \brief renvoie une liste avec un plateau en plus
* \param l Une liste
* \param x  Le nouveau plateau
* \return la liste associee
*/
list list_vADD(list l, plateau x);

/**
* \brief renvoie une liste avec un plateau en plus par effet
* \param pl Un pointeur vers une liste 
* \param x Le nouveau plateau
*/
void list_eADD(list* pl, plateau x);

/**
* \brief affiche une liste 
* \param  liste
*/
void print_list (list l);

/**
* \brief renvoie la taile d'une liste
* \param  l Une liste
* \return la taille assocée
*/
int longer_list(list l);

/**
* \brief Renverse l'ordre des éléments d'une liste
* \param  pl Un pointeur vers une liste
* \return la taille associée
*/
void list_reverse(list* pl);

/**
* \brief Récupère une tuiile d'une liste en fonction de sa position
* \param l une liste
* \param index Un entier donnant la position dans  las liste
* \param ret Un pointeur vers une tuile qui sert de copie
* \return 0 si on trouve la tuile -1 sinon
*/
int list_elementAt(list l, int index, plateau* ret);

/**
* \brief Récupère une tuiile d'une liste en fonction de son numéro
* \param l une liste
* \param id Un entier donnant le numéro de la tuile
* \param ret Un pointeur vers une tuile qui sert de copie
* \return 0 si on trouve la tuile -1 sinon
*/
int list_element_id(list l, int id, plateau* ret);

/**
* \brief Récupère une tuiile d'une liste en fonction de son numéro (comme list_element_id mais sans copie)
* \param l une liste
* \param id Un entier donnant le numéro de la tuile
* \param ret Un pointeur vers l'adresse d'une tuile qui va prendre la valeur de celle recherché
* \return 0 si on trouve la tuile -1 sinon
*/
int list_element_idbis(list l, int id, plateau** ret);

/**
* \brief efface un element de la liste
* \param  liste et element
* \return 0 si l'element n'existe pas 1 sinon
*/
int rm_element(list* pl, int idx);


void free_liste(list l);

list copie_list(list l);
#endif


