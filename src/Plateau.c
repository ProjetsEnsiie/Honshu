#include "Plateau.h"


terrain create_terrain(char type, int numero)
{
    terrain terr;
    terr.type = type;
    terr.numero = numero;
    return terr;
}

void print_terrain(terrain terr)
{
    printf("Case de type : %c, appartenant à la tuile : %d.\n", terr.type, terr.numero);
}

plateau create_plateau(int n, int m)
{
    plateau M;
    M.length=n;
    M.width=m;
    M.values=(terrain**)(malloc(n*sizeof(terrain*)));
    int i;
    int j;
    for (i=0; i<n; i++)
    {
        M.values[i]=(terrain*)(malloc(m*sizeof(terrain)));
        for (j=0; j<M.width; j++)
        {
                M.values[i][j].type = ' ';
                M.values[i][j].numero = -1;
        }
    }
    return M;
}

/*@REQUIRE :
@ASSIGN :
@ENSURE : plateau de taille random*/
plateau create_random_plateau(int maxx)
{
    srand(time(NULL));

    plateau M;
    M.length=rand()%maxx;
    M.width=rand()%maxx;
    M.values=(terrain**)(malloc(M.length*sizeof(terrain*)));
    int i;
    int j;
    for (i=0; i<M.length; i++)
    {
        M.values[i]=(terrain*)(malloc(M.width*sizeof(terrain)));
        for (j=0; j<M.width; j++)
        {
                M.values[i][j].type = ' ';
                M.values[i][j].numero = -1;
        }
    }

    return M;
}

/*@REQUIRE : i, j < taille du plateau
@ASSIGN :
@ENSURE : case correspondante*/
void lecture_terrain(plateau plat, int i, int j)
{
    printf("Case de type : %c, appartenant à la tuile : %d.\n", plat.values[i][j].type, plat.values[i][j].type);
}

char lecture_case(plateau plat, int i, int j)
{
    return plat.values[i][j].type;
}

int lecture_case_num(plateau plat, int i, int j)
{
    return plat.values[i][j].numero;
}
void print_plusieurs_plateaux(plateau* plat,int  nbplat){
  int i,j,k;
  for(j=0;j<3;j++){
    for(i=0;i<nbplat;i++){
        if(j==0){
          printf("%2d ", plat[i].values[0][0].numero);
        }
        else{
          printf("   "); /*index++*/
        }
        for(k=0;k<plat[i].width;k++){
          if (j<plat[i].length){
            printf("| %c |", plat[i].values[j][k].type);
          }
          else{
            printf("     ");
          }
        }
        printf(" ");
    }
    printf("\n" );
  }
}
void print_plateau(plateau plat)
{
    int i=0;
    int j=0;
    int n=plat.length;
    int m=plat.width;
    for (i=0; i<n; i++)
    {
        printf("|");
        for (j=0; j<m; j++)
        {
            printf("[%c]", plat.values[i][j].type);
        }
        printf("|\n");
    }
}
void free_plateau(plateau tuile){
  int i;
  for (i=0; i<tuile.length; i++)
  {
    free(tuile.values[i]);
  }
  free(tuile.values);
}

matrix create_matrix(int n)
{
    matrix M;
    M.size=n;
    M.values=(int**)(malloc(n*sizeof(int*)));
    int i;
    for (i=0; i<n; i++)
    {
        M.values[i]=(int*)(malloc(n*sizeof(int)));
    }
    return M;
}

matrix init_matrix(matrix M)
{
    int n=M.size;
    matrix N=create_matrix(n);
    int i;
    int j;
    for (i=0; i<n; i++)
    {
        for (j=0; j<n; j++)
        {
            N.values[i][j]=0;
        }
    }
    return N;
}

matrix random_matrix(int n, int max)
{
    matrix R=create_matrix(n);
    int i;
    int j;
    for (i=0; i<n; i++)
    {
        for (j=0; j<n; j++)
        {
            R.values[i][j]=rand()%max;
        }
    }
    return R;
}

void print_matrix(matrix M)
{
    int n=M.size;
    int i;
    int j;
    for (i=0; i<n; i++)
    {
        printf("\n|");
        for (j=0; j<n; j++)
        {
            printf("%3d", M.values[i][j]);
        }
        printf("   |");
    }
    printf("\n\n");
}
int contiens(int ** coords,int size,int x,int y){
  int i;
  for(i=0;i<=size;i++){
    if( coords[i][0]==x && coords[i][1]==y){
      return 1;
    }
  }
  return 0;
}
int village(plateau plat, int x,int y, int** visites, int profondeur){
  if ( y>0 && x>0 && x<plat.width && y<plat.length && contiens(visites,profondeur,x,y)==0 &&plat.values[x][y].type=='V'){
    visites[profondeur][0]=x;
    visites[profondeur][1]=y;
    profondeur++;
    /*parcours en profondeur de chaque case haut,bas,gauche,droite*/
    profondeur=village(plat,x,y-1,visites,profondeur);
    profondeur=village(plat,x-1,y, visites,profondeur);
    profondeur=village(plat,x+1,y, visites,profondeur);
    profondeur=village(plat,x,y+1, visites,profondeur);
  }
  return profondeur;
}
int plus_grand_village(plateau plat){
  int i,j,k,m,n,max,val,u;
  m=plat.length;
  n=plat.width;
  int **voisins = NULL;
  max=0,val=0;
  for(i=0;i<m;i++){
    for(j=0;j<n;j++){
      voisins = (int **) malloc(sizeof(int *) * m*n);
      for (k = 0; k < m*n; k++) {
        voisins[k] = (int *) malloc(sizeof(int) * m*n);
        for (u = 0; u < m*n; ++u)
        {
          voisins[k][u] = -1;
        }
      }
      val=village(plat, i,j, voisins, 0);
      max=max>val?max:val;
      for (k=0;k<m*n;k++){
        free(voisins[k]);
      }
      free(voisins);
      voisins = NULL;
    }
  }
  return max;
}


plateau copie_plateau(plateau p){
  plateau cp = create_plateau(p.length,p.width);
  int i, j;
  for (i = 0; i < p.length; ++i)
  {
    for (j = 0; j < p.width; ++j)
    {
      ((cp.values)[i][j]) =  ((p.values)[i][j]);
    }
  }
  return cp;
}