/**
*\file Plateau.h
*Ce fichier decrit les fonctions liées aux plateaux. Il contient 8 fonctions : (avec des fonctions en plus liées aux matrices)
*- create_terrain(char type, int numero) renvoie le terrain associé
*- create_plateau(int n, int m) renvoie un plateau de taille nxm
*- create_random_plateau(int maxx) renvoie un plateau de taille aleatoire
*- lecture_terrain(plateau plat, int i, int j) renvoie le type du terrain à la case (i, j) du plateau
*- lecture_case(plateau plat, int i, int j); renvoie le numero du terrain à la case (i, j) du plateau
*- lecture_case_num(plateau plat, int i, int j) affiche toutes les cases du plateau \a plat
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#ifndef PLATEAU_H
#define PLATEAU_H

typedef struct terrain terrain;
struct terrain
{
    char type;
    int numero;
};

typedef struct plateau plateau;
struct plateau
{
    int length;
    int width;
    terrain** values;
};


typedef struct matrix matrix;
struct matrix
{
    int size;
    int** values;
};

/**
* \brief Renvoie un terrain de type \a type et de numero \a numero
* \param type et numero
* \return le terrain associé
*/
terrain create_terrain(char type, int numero);

/**
* \brief Affiche un terrain
* \param terrain
* \return
*/
void print_terrain(terrain terr);

/**
* \brief Renvoie un plateau de taille nxm
* \param n et m
* \return le plateau associé
*/
plateau create_plateau(int n, int m);

/**
* \brief Renvoie un plateau de taille aleatoire
* \param maxx correspond au maximum de taille
* \return le plateau associé
*/
plateau create_random_plateau(int maxx);

/**
* \brief Affiche le terrain à la case (i, j) du plateau
* \param plateau , i et j
* \return
*/
void lecture_terrain(plateau plat, int i, int j);

/**
* \brief renvoie le type du terrain à la case (i, j) du plateau
* \param plateau , i et j
* \return le type associé
*/
char lecture_case(plateau plat, int i, int j);

/**
* \brief renvoie le numero du terrain à la case (i, j) du plateau
* \param plateau , i et j
* \return le numero associé
*/
int lecture_case_num(plateau plat, int i, int j);


/**
* \brief affiche toutes les cases du plateau \a plat
* \param plateau
* \return
*/
void print_plateau(plateau plat);

void print_plusieurs_plateaux(plateau* plat, int nbplat);

matrix create_matrix(int n);

matrix init_matrix(matrix M);

matrix random_matrix(int n, int max);

void print_matrix(matrix M);

int plus_grand_village(plateau plat);
int village(plateau plat, int x,int y, int** visites, int profondeur);
int contiens(int ** coords,int size,int x,int y);

void free_plateau(plateau tuile);

plateau copie_plateau(plateau p);
#endif
