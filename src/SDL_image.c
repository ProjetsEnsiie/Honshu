
#include <stdlib.h>

#include <stdio.h>

#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

#include "liste_tuile_surface.h"
#include "Plateau.h"
#include "tuile.h"
#include "insertion.h"
#include "Liste_tuiles.h"

#include "loadAndSave.h"
#include "pile.h"
#include "jeu.h"

#include "compte_point.h"
#include "solveur_naif.h"
/*gcc -o exec Plateau.c tuile.c insertion.c Liste_tuiles.c liste_tuile_surface.c loadAndSave.c pile.c jeu.c compte_point.c SDL_image.c  -lSDLmain -lSDL -lSDL_ttf -lm*/

unsigned long Color(int R, int G, int B)
{
    return 65536*R + 256*G + B;
}

void PutPixel(SDL_Surface *surface, int x, int y, unsigned long pixel)
{
    int bpp = surface->format->BytesPerPixel;
    unsigned char *p = (unsigned char *)surface->pixels + y * surface->pitch + x * bpp;
    if (bpp==4)
        *(unsigned long*)p = pixel;
}



void drawLine(SDL_Surface* surf,int x1,int y1, int x2,int y2, int R, int G, int B)  {
    unsigned long couleur = Color(R, G, B);
    int x,y;
    int Dx,Dy;
    int xincr,yincr;
    int erreur;
    int i;

    Dx = abs(x2-x1);
    Dy = abs(y2-y1);
    if(x1<x2)
        xincr = 1;
    else
        xincr = -1;
    if(y1<y2)
        yincr = 1;
    else
        yincr = -1;

    x = x1;
    y = y1;
    if(Dx>Dy)
    {
        erreur = Dx/2;
        for(i=0;i<Dx;i++)
        {
            x += xincr;
            erreur += Dy;
            if(erreur>Dx)
            {
                erreur -= Dx;
                y += yincr;
            }
            PutPixel(surf,x, y,couleur);
        }
    }
    else
    {
        erreur = Dy/2;
        for(i=0;i<Dy;i++)
        {
            y += yincr;
            erreur += Dx;

            if(erreur>Dy)
            {
                erreur -= Dy;
                x += xincr;
            }
            PutPixel(surf,x, y,couleur);
        }
    }
}



void Grille(SDL_Surface *ecran,int n, int w, int h) {

    n=n+1;
    int x=w/2,y=h/2,u=54;
    int offsetY=-(y*0.25);
    int xa,ya;
    int x1 = x-u*(n/2);
    int y1 = y+offsetY-u*(n/2);
    int x2 = x+u*(n/2);
    int y2 = y+offsetY+u*(n/2);

    for(x=x1;x<x2;x+=u){
        drawLine(ecran,x,y1,x,y2-u,0,0,0);
    }

    for(y=y1;y<y2;y+=u){
        drawLine(ecran,x1,y,x2-u,y,0,0,0);
    }
}

SDL_Rect Case_Grille(SDL_Surface *ecran, int srx, int sry,int n,int w,int h) {

    SDL_Rect rec;

    int i = 0;
    int j = 0;
    n = n+1;
    int x=w/2,y=h/2,u=54;

    int offsetY=-(y*0.25);
    int xa,ya;
    int x1 = x-u*(n/2);
    int y1 = y+offsetY-u*(n/2);
    int x2 = x+u*(n/2);
    int y2 = y+offsetY+u*(n/2);

    // lignes verticales
    for(x=x1;x<x2;x+=u){

        for(y=y1;y<y2;y+=u){

            if (srx > x && srx <= (x+u) && sry > y && sry <= (y+u)) {

                rec.x = i;
                rec.y = j%n;
                return rec;




            }
            j++;

        }
        i++;
    }

    rec.x = -1;
    rec.y = -1;

    return rec;
}


SDL_Surface * genererPlateau(plateau plt, int nb_tuile,Uint32 Rmask,Uint32 Gmask,Uint32 Bmask,Uint32 Amask){
    
    int m = plt.width;
    int n = plt.length;
    int w=m, l=n;
    SDL_Surface *plateau = NULL;
    plateau=  SDL_CreateRGBSurface(SDL_HWSURFACE,w*54,l*54,32,Rmask, Gmask, Bmask, Amask);
    
    /*SDL_FillRect(plateau,NULL,SDL_MapRGB(plateau->format,255,255,255));*/

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            SDL_Rect positionCase;
            positionCase.x = (j)*54;
            positionCase.y = (i)*54;
            if (plt.values[i][j].type=='L')
            {
                SDL_BlitSurface(SDL_LoadBMP("img/Officiel/Lac.bmp"),NULL,plateau,&positionCase);
            }
            else if (plt.values[i][j].type=='F')
            {
                SDL_BlitSurface(SDL_LoadBMP("img/Officiel/Foret.bmp"),NULL,plateau,&positionCase);
            }
             else if (plt.values[i][j].type=='V')
            {
                SDL_BlitSurface(SDL_LoadBMP("img/Officiel/Ville.bmp"),NULL,plateau,&positionCase);
            }
            else if (plt.values[i][j].type=='U')
            {
                SDL_BlitSurface(SDL_LoadBMP("img/Officiel/Usine.bmp"),NULL,plateau,&positionCase);
            }
            else if (plt.values[i][j].type=='P')
            {
                SDL_BlitSurface(SDL_LoadBMP("img/Officiel/Plaine.bmp"),NULL,plateau,&positionCase);
            }
            else if (plt.values[i][j].type=='R')
            {
                SDL_BlitSurface(SDL_LoadBMP("img/Officiel/Ressource.bmp"),NULL,plateau,&positionCase);
            }
        }
    }
    return plateau;
}



int replaceTuile(list_tsrf liste_surf_t,int id_cur, SDL_Surface* zozor_cur, int nb_tuile, int yinit,int xinit ){
    int err;
    err = surface_at_id(liste_surf_t,id_cur,&zozor_cur);
    if (err == 1){
        err =  set_pos_at_id(&liste_surf_t, id_cur, xinit , yinit);
        
    }
    return err;
}


void affiche_score(SDL_Surface* ecran, TTF_Font *police,SDL_Color couleur,int newScore, int scoreSolv){
    SDL_Surface* scoretxt = TTF_RenderText_Solid(police, "Votre score est :", couleur);
    SDL_Surface* scoresolvtxt = TTF_RenderText_Solid(police, "Le meilleur score est :", couleur);
    char str[12];
    sprintf(str, "%d", newScore);
    SDL_Surface* score = TTF_RenderText_Solid(police, str, couleur);

    sprintf(str, "%d", scoreSolv);
    SDL_Surface* scores = TTF_RenderText_Solid(police, str, couleur);

    SDL_Rect position_score;
    position_score.x = 100;
    position_score.y = 100;
    SDL_BlitSurface(scoretxt, NULL, ecran, &position_score);
    position_score.x = 300;
    position_score.y = 100;
    SDL_BlitSurface(score, NULL, ecran, &position_score);
    position_score.x = 100;
    position_score.y = 200;
    SDL_BlitSurface(scoresolvtxt, NULL, ecran, &position_score);
    position_score.x = 300;
    position_score.y = 200;
    SDL_BlitSurface(scores, NULL, ecran, &position_score);

}



void update(SDL_Surface* imageDeFond,SDL_Surface* plat, SDL_Surface* ecran,SDL_Rect* positionFond,SDL_Rect* positionPlat, list_tsrf liste_surf_t,
    int taille_grille,int width,int height, TTF_Font *police,SDL_Color couleur, int newScore,int scoreSolv){
    SDL_BlitSurface(imageDeFond, NULL, ecran, positionFond);
    print_list_tsrf(liste_surf_t,ecran);
    SDL_BlitSurface(plat, NULL, ecran, positionPlat);
    print_list_tsrf(liste_surf_t,ecran);
    affiche_score(ecran, police, couleur,newScore,scoreSolv);
    Grille(ecran,taille_grille,width,height);
    SDL_Flip(ecran);
}

void updateFin(SDL_Surface* imageDeFond,SDL_Surface* plat, SDL_Surface* ecran,SDL_Rect* positionFond,SDL_Rect* positionPlat, list_tsrf liste_surf_t,
    int taille_grille,int width,int height, TTF_Font *police,SDL_Color couleur, int newScore,int scoreSolv){
    SDL_BlitSurface(imageDeFond, NULL, ecran, positionFond);
    print_list_tsrf(liste_surf_t,ecran);
    SDL_BlitSurface(plat, NULL, ecran, positionPlat);
    print_list_tsrf(liste_surf_t,ecran);
    affiche_score(ecran, police, couleur,newScore,scoreSolv);

    SDL_Color couleur2 = {255,0,0};
    SDL_Rect position_score2;
    TTF_Font *police2 = NULL;
    police2 = TTF_OpenFont("arial.ttf", 60);
    SDL_Surface* msgfin =NULL;
    if(newScore<scoreSolv) msgfin = TTF_RenderText_Solid(police, "Tu es NULL !", couleur2);
    else msgfin = TTF_RenderText_Solid(police, "Bravo !", couleur2);
    
    position_score2.x = 500;
    position_score2.y = 200;

    SDL_BlitSurface(msgfin, NULL, ecran, &position_score2);


    Grille(ecran,taille_grille,width,height);
    SDL_Flip(ecran);
}

int get_free_x(list_tsrf srf,int max){
    int x = 0;
    int m = max;
    int p = max-120;
    list_tsrf cur = srf;
    for (p=max-120;p>=28;p=p-120 ){
       
        x = 0;
        while(cur != NULL){
            
            if ((cur->position).x == p) {
                x=-1;
                break;
            }
            
            cur = cur->next;
        }
        if (x==0) return p;
        cur = srf;
    }
    
    return m;
}

/*gcc -o exec src/SDL_image.c -lSDLmain -lSDL*/
int main(int argc, char *argv[])

{

    Uint32 tlRmask = 0x000000FF, tlGmask = 0x0000FF00, tlBmask= 0x00FF0000, tlAmask= 0xFF000000,pltRmask= 0x0000FF00, pltGmask= 0x000000FF, pltBmask= 0x00FF0000, pltAmask = 0xFF000000;

    

    int newScore = 0;

    int width = 1600;
    int height = 980;
    int nb_tuile,nb_tuile_start;
    int taille_grille;

    plateau gs;
    list listes = create_list();
    Stack tpiles;
    Init_Stack(&tpiles);
    load_partie("tuiles.txt", "partie1.txt", &gs, &listes, &tpiles);

    
    int scoreSolv = solveur_naif(gs, listes);

    plateau g;
    list liste = create_list();
    Stack tpile;
    Init_Stack(&tpile);
    load_partie("tuiles.txt", "partie1.txt", &g, &liste, &tpile);
    nb_tuile = longer_list(liste);
    nb_tuile_start = nb_tuile;
    taille_grille = g.length;
    plateau* tuile_rot = NULL;

    int err, id_cur = -1;
    list_tsrf lSurf = create_list_tsrf();
    SDL_Surface* zozor_cur = NULL;

    SDL_Surface *ecran = NULL, *imageDeFond = NULL, *imageDeFondtmp = NULL;

    SDL_Rect positionFond, positioncurrent;
    SDL_Rect positionTuile;
    SDL_Rect positionPlat;
    SDL_Rect positionCase1;
    SDL_Surface *tuile = NULL;
    SDL_Surface *case1 = NULL;

    SDL_Event event; /* La variable contenant l'événement */
    int continuer = 1; /* Notre booléen pour la boucle */
    int srx, sry = 0; /*position de la souris*/

    positionFond.x = 0;
    positionFond.y = 0;

    positionCase1.x = 0;
    positionCase1.y = 0;


    positioncurrent.x = 0;
    positioncurrent.y = 0;

    SDL_Init(SDL_INIT_VIDEO);
    TTF_Init();

    int onclick = 0;

    ecran = SDL_SetVideoMode(width, height, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);

    SDL_WM_SetCaption("Chargement d'images en SDL", NULL);


    /* Chargement d'une image Bitmap dans une surface */

    imageDeFondtmp = SDL_LoadBMP("Marron-bicubic_grd.bmp");
    imageDeFond = SDL_DisplayFormat ( imageDeFondtmp );
    SDL_FreeSurface(imageDeFondtmp);

    TTF_Font *police = NULL;
    police = TTF_OpenFont("arial.ttf", 20);
    SDL_Color couleur = {255,250,170};
    /* On blitte par-dessus l'écran */

    SDL_BlitSurface(imageDeFond, NULL, ecran, &positionFond);


    /*chargement du plateau de jeu de débart */
    SDL_Surface * plat = genererPlateau(g,nb_tuile, pltRmask, pltGmask, pltBmask, pltAmask);
    int x=width/2,y=height/2,n=taille_grille+1,u=54;
    int offsetY=-(y*0.25);
    int x1 = x-u*(n/2);
    int y1 = y+offsetY-u*(n/2);
    positionPlat.x=x1;
    positionPlat.y=y1;
    SDL_BlitSurface(plat, NULL, ecran, &positionPlat);
    /* Chargement et blittage de Zozor sur la scène */



    list_tsrf liste_surf_t = create_list_tsrf();

    int xinit,yinit;
    
    xinit = 28;
    yinit = 750;
    list curlist = liste;
    list_tsrf curList  =  liste_surf_t;
    plateau ptmp;
    while (curlist != NULL){
        list_tsrf_eADD(&liste_surf_t, genererPlateau(curlist->value,nb_tuile, tlRmask, tlGmask, tlBmask, tlAmask),xinit,yinit);
        
        xinit = xinit + 120;
        
        curlist = curlist->next;
    }
    int max = xinit;


    

    print_list_tsrf(liste_surf_t,ecran);


    Grille(ecran,taille_grille,width,height);

    SDL_Flip(ecran);

    while (continuer) /* TANT QUE la variable ne vaut pas 0 */

    {
        

        SDL_WaitEvent(&event); /* On attend un événement qu'on récupère dans event */

        switch(event.type) /* On teste le type d'événement */

        {

            case SDL_QUIT: /* Si c'est un événement QUITTER */

                continuer = 0; /* On met le booléen à 0, donc la boucle va s'arrêter */

                break;



            case SDL_KEYDOWN:

                switch(event.key.keysym.sym)

                {

                    case SDLK_RIGHT: 
                        err = surface_id_at_pos(liste_surf_t, srx, sry,&id_cur,160, 108);
                        if (err == 1){
                            err = surface_at_id(liste_surf_t,id_cur,&zozor_cur);
                            if (err == 1){                    
                                list_elementAt(liste, nb_tuile-1 - id_cur , &ptmp);
                                tuile_rot = NULL;
                                list_element_idbis(liste, ((ptmp.values)[0][0]).numero, &tuile_rot);
                                rotation_droite(tuile_rot);                            
                                err =  set_surface_at_id(&liste_surf_t, id_cur, genererPlateau(*tuile_rot,nb_tuile, tlRmask, tlGmask, tlBmask, tlAmask) );
                                update( imageDeFond, plat,  ecran, &positionFond, &positionPlat,  liste_surf_t,  taille_grille, width, height,police, couleur,newScore,scoreSolv);
                            }
                        }

                        break;

                    case SDLK_LEFT: 

                        err = surface_id_at_pos(liste_surf_t, srx, sry,&id_cur,160, 108);
                        if (err == 1){
                            err = surface_at_id(liste_surf_t,id_cur,&zozor_cur);
                            if (err == 1){
                                list_elementAt(liste, nb_tuile-1 - id_cur , &ptmp);
                                tuile_rot = NULL;
                                list_element_idbis(liste, ((ptmp.values)[0][0]).numero, &tuile_rot);
                                rotation_gauche(tuile_rot);                              
                                err =  set_surface_at_id(&liste_surf_t, id_cur, genererPlateau(*tuile_rot,nb_tuile, tlRmask, tlGmask, tlBmask, tlAmask) );
                                update( imageDeFond, plat,  ecran, &positionFond, &positionPlat,  liste_surf_t,  taille_grille, width, height,police, couleur,newScore,scoreSolv);
                            }
                        }

                        break;
                    case SDLK_u:
                        if (nb_tuile<nb_tuile_start){
                            undo( &g,g.length, &liste, &tpile);
                            nb_tuile++;
                            xinit = get_free_x(liste_surf_t,max);

                            list_tsrf_eADD_END(&liste_surf_t, genererPlateau(liste->value,nb_tuile, tlRmask, tlGmask, tlBmask, tlAmask),xinit,yinit);
                            
                            plat = genererPlateau(g,nb_tuile, pltRmask, pltGmask, pltBmask, pltAmask);
                            print_plateau(g);
                            newScore = points(g);
                            update( imageDeFond, plat,  ecran, &positionFond, &positionPlat,  liste_surf_t,  taille_grille, width, height,police, couleur,newScore,scoreSolv);
                        }
                    break;

                }




            break;

            case SDL_MOUSEBUTTONDOWN:
                if (event.button.button!=SDL_BUTTON_WHEELUP && event.button.button!=SDL_BUTTON_WHEELDOWN){
                    onclick = 1;

                } 
                
                

                break;

            case SDL_MOUSEBUTTONUP:
                if (event.button.button!=SDL_BUTTON_WHEELUP && event.button.button!=SDL_BUTTON_WHEELDOWN) {

                    onclick = 0;
                    
                    err = surface_id_at_pos(liste_surf_t, srx, sry,&id_cur,160, 108);
                    list_elementAt(liste, nb_tuile-1 - id_cur , &ptmp);
                    print_plateau(ptmp);
                    if(positioncurrent.x == -1 )
                    {
                        
                        if (err == 1){
                            
                            err = surface_at_id(liste_surf_t,id_cur,&zozor_cur);
                            if (err == 1){
                                err =  replaceTuile( liste_surf_t, id_cur,  zozor_cur,  nb_tuile,  yinit,xinit );
                                update( imageDeFond, plat,  ecran, &positionFond, &positionPlat,  liste_surf_t,  taille_grille, width, height,police, couleur,newScore,scoreSolv);
                            }
                        }    
                    }
                    else{
                        if (err == 1){
                            err = poser_tuile(&liste, ((ptmp.values)[0][0]).numero, &g, positioncurrent.y, positioncurrent.x, &tpile);
                            if (err != 0){
                                err =  replaceTuile( liste_surf_t, id_cur,  zozor_cur,  nb_tuile,  yinit,xinit );
                                
                            }
                            else{
                                plat = genererPlateau(g,nb_tuile, pltRmask, pltGmask, pltBmask, pltAmask);
                                nb_tuile--;
                                rm_sfr_element(&liste_surf_t, id_cur);
                                newScore = points(g);

                            }
                            update( imageDeFond, plat,  ecran, &positionFond, &positionPlat,  liste_surf_t,  taille_grille, width, height,police, couleur,newScore,scoreSolv);
                            print_plateau(g);
                        }
                    }
                }   

                

                
                    

                break;


            case SDL_MOUSEMOTION:
                srx = event.button.x;
                sry = event.button.y;

                positioncurrent = Case_Grille(ecran, srx, sry,taille_grille,width,height);

                
                int id = 0;
                curList = liste_surf_t;
                
                while (curList != NULL){
                    if(event.button.x > (curList->position).x  && event.button.x < ((curList->position).x + 108) && event.button.y > (curList->position).y && event.button.y < ((curList->position).y + 160))
                    {
                                     
                        if (onclick == 0) xinit = (curList->position).x;
                        if( onclick ) {

                            (curList->position).x = event.button.x - 108/4;

                            (curList->position).y = event.button.y - 160/6;
                            update( imageDeFond, plat,  ecran, &positionFond, &positionPlat,  liste_surf_t,  taille_grille, width, height,police, couleur,newScore,scoreSolv);


                            list_elementAt(liste, id , &ptmp);
                            
                            break;
                        }
                        
                    }
                    id++;
                    curList = curList->next;
                }

            break;
            

        }


        if (nb_tuile == 0) {
            updateFin( imageDeFond, plat,  ecran, &positionFond, &positionPlat,  liste_surf_t,  taille_grille, width, height,police, couleur,newScore,scoreSolv);

        }
        
    }


    SDL_FreeSurface(imageDeFond); /* On libère la surface */

    SDL_Quit();


    return EXIT_SUCCESS;

}
