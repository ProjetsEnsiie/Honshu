#include <stdlib.h>

#include <stdio.h>

#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

#include "liste_tuile_surface.h"
#include "Plateau.h"
#include "tuile.h"
#include "insertion.h"
#include "Liste_tuiles.h"

#include "loadAndSave.h"
#include "pile.h"
#include "jeu.h"

#include "compte_point.h"



#ifndef __SDL_image_h__
#define __SDL_image_h__

unsigned long Color(int R, int G, int B);
void PutPixel(SDL_Surface *surface, int x, int y, unsigned long pixel);
void drawLine(SDL_Surface* surf,int x1,int y1, int x2,int y2, int R, int G, int B);
void Grille(SDL_Surface *ecran,int n, int w, int h);
SDL_Rect Case_Grille(SDL_Surface *ecran, int srx, int sry,int n,int w,int h);
SDL_Surface * genererPlateau(plateau plt, int nb_tuile,Uint32 Rmask,Uint32 Gmask,Uint32 Bmask,Uint32 Amask);
int replaceTuile(list_tsrf liste_surf_t,int id_cur, SDL_Surface* zozor_cur, int nb_tuile, int yinit,int xinit );
void affiche_score(SDL_Surface* ecran, TTF_Font *police,SDL_Color couleur,int newScore);
void update(SDL_Surface* imageDeFond,SDL_Surface* plat, SDL_Surface* ecran,SDL_Rect* positionFond,SDL_Rect* positionPlat, list_tsrf liste_surf_t,int taille_grille,int width,int height, TTF_Font *police,SDL_Color couleur, int newScore);
int get_free_x(list_tsrf srf,int max);
int main(int argc, char *argv[]);

#endif
