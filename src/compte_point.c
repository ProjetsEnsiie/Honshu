#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "Plateau.h"
#include "tuile.h"
#include "insertion.h"
#include "Liste_tuiles.h"
#include "loadAndSave.h"
#include "pile.h"
#include "jeu.h"

#include "compte_point.h"

/*Fonction de création*/
list_couple listc_create() {
	return NULL;
}

/*Fonction pour savoir si une liste est vide ou non*/
int listc_isEmpty(list_couple l) {
	return l == NULL;
}


void free_nodec(list_couple* V) {
  list_couple next;
  if (!listc_isEmpty(*V)) {
    next=(*V)->next;
    free(*V);
    *V=next;
  }
}

/*Fonction pour ajouter un couple à la liste*/
list_couple listc_vAdd(struct couple c, list_couple l) {
	list_couple newList = (list_couple)malloc(sizeof(struct nodec));
	newList->c.x =c.x ;
	newList->c.y = c.y;
	newList->next = l;
	return newList;
}
/*Fonction auxiliare pour ajouter un couple à la liste*/
void listc_eAdd(list_couple* pl, struct couple c) {
	*pl = listc_vAdd(c, *pl);
}

void free_listc(list_couple* l) {
   list_couple* next;
   while (!listc_isEmpty(*l)) {
     next=&(*l)->next;
     free(*l);
     l=next;
     free(next);
   }
 }

void compte_nb_terrains(plateau p,int* t) {
  int n,m,i,j;
  n=p.length;
  m=p.width;
  for (i=0;i<n;i++) {
    for (j=0;j<m;j++) {
      if (p.values[i][j].type=='V') {
	t[0] +=1;
      }
      else if (p.values[i][j].type=='F') {
	t[1] +=1;
      }
      else if (p.values[i][j].type=='L') {
	t[2] +=1;
      }
      else if (p.values[i][j].type=='U') {
	t[3] +=1;
      }
      else if (p.values[i][j].type=='R') {
	t[4] +=1;
      }
    }
  }
}



int max(int a, int b) {
  if (a>b) {
    return a;
  }
  else {
    return b;
  }
}

int min(int a, int b) {
  if (a<b) {
    return a;
  }
  else {
    return b;
  }
}

int points (plateau p) {
  int pts;
  int* t=calloc(5,sizeof(int));
  compte_nb_terrains(p,t);
  pts=2*t[1] + 3*max(t[2]-1,0) + 4*min(t[3],t[4]) + plus_grand_village( p);
  free(t);
  return(pts);
}
