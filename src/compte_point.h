#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "Plateau.h"
#include "tuile.h"
#include "insertion.h"
#include "Liste_tuiles.h"
#include "loadAndSave.h"
#include "pile.h"
#include "jeu.h"

#ifndef COMPTE_POINT_H
#define COMPTE_POINT_H

/*coodronnées*/
struct couple {
  int x;
  int y;};

struct nodec {
        struct couple c;
	struct nodec* next;
};


typedef struct nodec* list_couple;

list_couple listc_create();

int listc_isEmpty(list_couple l);;

void free_nodec(list_couple* V);

list_couple listc_vAdd(struct couple c, list_couple l) ;

void listc_eAdd(list_couple* pl, struct couple c);

void free_listc(list_couple* l);

void compte_nb_terrains(plateau p,int* t);

int max(int a, int b);

int min(int a, int b) ;

int points (plateau p);
#endif
