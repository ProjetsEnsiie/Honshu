#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#include <math.h>
#include "genetique.h"

typedef liste_main* mains;
mains create_mains(){
	return NULL;
}
void del_mains(mains p){
	while (!mains_is_empty(p)){
        free_liste(p->value);
        mains tmp = p;
        p = p->next;
        free(tmp);
    }
    free(p);
}
int mains_is_empty(mains p){
	return (p==NULL);
}
mains mains_vADD(mains p, list x){
	mains newliste_main = (mains)malloc(sizeof(liste_main));
    newliste_main->value=x;
    newliste_main->next=p;
    return newliste_main;
}
void mains_eADD(mains* p, list x){
	*p = mains_vADD(*p, x);
}
void print_mains (mains p){
	mains cur = p;
	while(!mains_is_empty(cur)){
		print_list(cur->value);
		cur = cur->next;
		printf("\n\n\n\n");
	}
}
int longer_mains(mains p){
	int i=0;
    mains currentmains=p;
    while (currentmains!=NULL)
    {
        i++;
        currentmains=currentmains->next;
    }
    return i;
}


typedef ind* population;
population create_population(){
	return NULL;
}
void del_population(population p){
	while (!population_is_empty(p)){
        del_couche(&(p->value));
        population tmp = p;
        p = p->next;
        free(tmp);
    }
    free(p);
}
int population_is_empty(population p){
	return (p==NULL);
}
population population_vADD(population p, couche x){
	population newind = (population)malloc(sizeof(ind));
    newind->value=x;
    newind->next=p;
    return newind;
}
void population_eADD(population* p, couche x){
	*p = population_vADD(*p, x);
}
void print_population (population p){
	population cur = p;
	int i = 1;
	while(!population_is_empty(cur)){
		printf("individu : %d\n",i );
		print_couche(&(cur->value));
		cur = cur->next;
		printf("\n\n\n\n");
		i++;
	}
}
int longer_population(population p){
	int i=0;
    population currentpopulation=p;
    while (currentpopulation!=NULL)
    {
        i++;
        currentpopulation=currentpopulation->next;
    }
    return i;
}
int population_elementAt(population l, int index, couche* ret)
{
    population current=l;
    while (current!=NULL && index>0)
    {
        index--;
        current=current->next;
    }
    if (current==NULL)
    {
        return 0;
    }
    else
    {
        *ret=current->value;
        return 1;
    }
}





couche create_couche(int l, int c){
	couche m;
	m.mat =(float**) malloc(l*sizeof(float*));
	int i,j;
	for (i = 0; i < l; ++i)
	{
		(m.mat)[i] = (float*) malloc(c*sizeof(float));
		for (j = 0; j < c; ++j)
		{
			(m.mat)[i][j] = -1.;
		}
	}
	m.l = l;
	m.c = c;
	return m;

}

void reset_couche(couche* X){
	int i,j;
	for (i = 0; i < X->l; ++i)
	{
		for ( j = 0; j < X->c; ++j)
		{
			X->mat[i][j] = -1;
		}
	}
}

int copie(couche X, couche* Y){
	if (X.c != Y->c || X.l != Y->l ) return -1;
	int i,j;
	for ( i = 0; i < Y->l; ++i)
	{
		for (j = 0; j < Y->c; ++j)
		{
			(Y->mat)[i][j] = (X.mat)[i][j];
		}
	}
	return 0;
}

void del_couche(couche* m){
	int i;
	for (i = 0; i < m->l; ++i)
	{
		free((m->mat)[i]);
	}
	free(m->mat);
}

void print_couche(couche* m){
	int i,j;
	for ( i = 0; i < m->l; ++i)
	{
		for (j = 0; j < m->c; ++j)
		{
			printf("%f ",(m->mat)[i][j] );
		}
		printf("\n");
	}
	printf("\n");
}

void rand_couche(couche* m){
	
	int i,j;
	float a = -1.;
	float b = 1.;
	for ( i = 0; i < m->l; ++i)
	{
		for (j = 0; j < m->c; ++j)
		{
			(m->mat)[i][j] =(float)rand()/(float)RAND_MAX  * (b-a) + a;
		}
	}
	
}

couche feed_forward_step(couche X, couche O){
	int n = X.c;
	int m = O.l;
	couche r;
	r = create_couche(1,m+1);
	if (X.l != 1) {printf("erreur taille de X\n");return r;};
	if (O.c != n) {printf("erreur tailles O et X\n");return r;};
	int i,j;
	r.mat[0][0] = 1;
	for (i = 0; i < m; ++i)
	{
		for ( j = 0; j< n; ++j)
		{
			(r.mat)[0][i+1] += ((X.mat)[0][j])*((O.mat)[i][j]);
	
		}
	}
	return r;

}

float rand_normale1(float mu, float sigma){
	
	float r,x,y,z;
	do
	{
		x = 2*(rand()/(float)RAND_MAX) -1;
		y = 2*(rand()/(float)RAND_MAX) -1;
		r = x*x + y*y;
	} while (r>=1);
	z = sqrt(-2*log(r)/r);
	return mu + sigma*z*x;
}

float rand_normale(float mu, float sigma){
	
	float x,y,r;
	
	x = (rand()/(float)RAND_MAX) ;
	y = (rand()/(float)RAND_MAX) ;

	
	r = sqrt(-2*log(x))*cos(2*3.14159265359*y);
	return (mu + sigma*r);
}

void mutation_couche_normale(couche* m, float* sigma,float to){
	int i,j;
	
	for ( i = 0; i < m->l; ++i)
	{
		for (j = 0; j < m->c; ++j)
		{
			m->mat[i][j] = m->mat[i][j] + rand_normale(0,*sigma);
		}
	}
	*sigma = (*sigma)*exp(to*rand_normale(0,1));
}

float index_of_max(couche C,int* idx){
	int i;
	float min = -99999999999.;
	for (i = 0; i< C.c; ++i)
	{
		float v = (C.mat)[0][i];
		if (v>min){
			min = v;
			*idx = i;
		}
	}
	return min;
}


int terrain_to_int(char c){
	if (c == 'P') return 1;
	else if (c == 'F') return 2;
	else if (c == 'L') return 3;
	else if (c == 'V') return 4;
	else if (c == 'R') return 5;
	else if (c == 'U') return 6;
	else return 0;
}

void tuile_to_intvec(couche* X,int start,char* ctuile){
	int i;
	for (i = 0; i < 6; ++i)
	{
		X->mat[0][i+start] = (float) terrain_to_int(ctuile[i]);
	}
}

void grille_to_intvec(couche* X,int start, plateau grille){
	int taille_grille = grille.length;
	int j,k;
	for (j=0;j<taille_grille;j++){
		for ( k = 0; k < taille_grille; ++k)
		{
			X->mat[0][k+j*8+start] = (float) terrain_to_int(grille.values[k][j].type);
		}
	}
}

void modele1(int taille_grille, int nb_tuile, couche* m1, couche* m2, couche* mf){
	int taille_m1_c = nb_tuile*6 + taille_grille*taille_grille + 6 + 1;/*tuilecur+toutetuile+grille+biais*/
	int taille_mf_l = taille_grille*taille_grille+2+1; /*position grille,rotd,rotg,rien*/

	int taille_m2_l = taille_mf_l+5;
	int taille_m1_l = taille_m2_l+10;

	int taille_m2_c = taille_m1_l +1 ;
	int taille_mf_c = taille_m2_l+1;

	*m1 = create_couche(taille_m1_l, taille_m1_c);
	*m2 = create_couche(taille_m2_l, taille_m2_c);
	*mf = create_couche(taille_mf_l, taille_mf_c);
	

	rand_couche(m1);
	rand_couche(m2);
	rand_couche(mf);
}

couche feed_forward_mdl_1(couche X, couche m1, couche m2, couche mf){
	couche c1, c2, c3;
	c1 = feed_forward_step( X ,m1);
	sigmoid(&c1);

	c2 = feed_forward_step( c1,m2);
	sigmoid(&c2);

	c3 = feed_forward_step(c2,mf);
	sigmoid(&c3);

	del_couche(&c1);
	del_couche(&c2);
	return c3;
}

void form_data_mdl_1(couche* X, plateau grille, plateau tuile_cur,list hand){
	
	int taille_main = longer_list(hand);
	int i = 0;
	char* ctuile ;

	X->mat[0][0] = 1;

	ctuile = get_string_tuile(tuile_cur);
	tuile_to_intvec(X,i,ctuile);
	free(ctuile);
	list l = hand;
	while(!list_is_empty(l))
	{
		ctuile = get_string_tuile(l->value);
		tuile_to_intvec(X,i,ctuile);
		l= l->next;
		i=i+6;
		free(ctuile);
	}

	grille_to_intvec(X,(taille_main+1)*6,grille);

	

}
void save_data_mdl1(){
	printf("ok\n" );
}

void sigmoid(couche* m){
	int i;
	for (i = 0; i < m->c; ++i)
	{
		m->mat[0][i] = 1/( 1 + exp(-1*(m->mat[0][i])) );
	}
}


void cross_over(couche pere, couche mere, couche* fils){
	int i,j,p;
	for (i = 0; i < pere.l; ++i)
	{
		p = rand()%2;
		if (p == 0){
			for (j = 0; j < pere.c; ++j)
			{
				fils->mat[i][j] = pere.mat[i][j];
			}
		}
		else{
			for (j = 0; j < pere.c; ++j)
			{
				fils->mat[i][j] = mere.mat[i][j];
			}
		}
	}
}

int* tri(int* l, int sz){
	int* idx = (int*) malloc(sz*sizeof(int));
	int i,j,k,tmp1,tmp2,itmp1,itmp2;
	for (i = 0; i < sz; ++i)
	{
		idx[i] = i;
	}
	for (i = 0; i < sz; ++i)
	{
		for (j = 0; j < i; ++j)
		{
			if(l[i]<l[j]){
				tmp1 = l[j];
				l[j] = l[i];

				itmp1 = idx[j];
				idx[j] = idx[i];
				for (k = j+1; k <= i; ++k)
				{
					tmp2 = l[k];
					l[k] = tmp1;
					tmp1 = tmp2;

					itmp2 = idx[k];
					idx[k] = itmp1;
					itmp1 = itmp2;
				}
			}
		}
	}
	return idx;

}

int* extract_rand(int* l, int sz, int new_sz){
	
	int* cl = (int *) malloc(new_sz*sizeof(int));
	int* distribution_idx = (int*) malloc(new_sz*sizeof(int));
	int i;
	for (i = 0; i < new_sz; ++i)
	{
		distribution_idx[i] = -1;
	}
	int j = -1;
	for (i = 0; i < new_sz; ++i)
	{
		while(e_in(distribution_idx,new_sz,j)==1){

			j = rand()%(sz);
		}
		distribution_idx[i] = j;
	}
	for (i = 0; i < new_sz; ++i)
	{
		cl[i] = l[distribution_idx[i]];
	}
	free(distribution_idx);
	return cl;
}

int* complement(int* l, int sz){
	int* cl = (int *) malloc(sz * sizeof(int));
	int i;
	int j = 0;
	for (i = 0; i < sz; ++i)
	{
		cl[i] = -1;
	}
	for (i = 0; i < sz; ++i)
	{
		j = 0;
		while (e_in(l, sz, j) != 0 || e_in(cl, sz, j) != 0) j++;
		cl[i] = j;
	}

	return cl;
}

int e_in(int* l,int sz, int elt){
	int i;
	for (i = 0; i < sz; ++i)
	{
		if (l[i] == elt) return 1;
	}
	return 0;
}

int* selection(int* fit, int sz){
	int k = sz/2; /* sz%2 == 0 */
	int p = 2*k/3; /* sz%6 == 0 */
	int i,a;
	int* idx = tri(fit, sz);
	int* distribution_idx = (int*) malloc(k*sizeof(int));
	int* select_idx = (int*) malloc(k*sizeof(int));
	for (i = 0; i < k; ++i)
	{
		distribution_idx[i] = -1;
	}

	a = -1;

	for (i = 0; i < k/3; ++i)
	{	
		do{
			a = rand()%(k);
		}while(e_in(distribution_idx,k,a)==1);
		distribution_idx[i] = a;
	}

	for (i = k/3; i < k/3 + p/3; ++i)
	{
		do{
			a = rand()%(k/2) + k;
		}while(e_in(distribution_idx,k,a)==1);
		distribution_idx[i] = a;
	}

	for (i = k/3 + p/3; i < k; ++i)
	{
		do{
			a = rand()%(k/2) + k+ k/2;
		}while(e_in(distribution_idx,k,a)==1);
		distribution_idx[i] = a;
	}

	for (i = 0; i < k; ++i)
	{
		select_idx[i] = idx[distribution_idx[i]];
	}
	free(distribution_idx);
	free(idx);
	return select_idx;
}


int readTo2(FILE* fichier, float* f, char end1, char end2){

	int c2 = 0;
	char c1[20] = "";
	int i=0;
	while(c2 != (int) end1 || c2 != (int) end2){
		c2 = fgetc(fichier);
		c1[i] = c2;
		i++;
	}

	*f =atof(c1);
	if (c2 == (int) end2) return 0;
	else if (c2 == (int) '*') return -1;
	else if (c2 == (int) '!') return -2;
	return 1;
}


void save_population(population p, char* chaine){
	FILE* fichier = fopen(chaine,"w");
	int i,j;
	char c[100] = "";
	population cur = p;

	while(!population_is_empty(cur)){
		for (i = 0; i < (cur->value).l ; ++i)
		{
			for (j = 0; j < (cur->value).c ; ++j)
			{
				sprintf(c,"%f",(cur->value).mat[i][j]);
				fputs(c,fichier);
				fputc(' ',fichier);
			}
			fputc('\n',fichier);
		}
		fputc('*',fichier);
		fputc('\n',fichier);
		cur = cur->next;
	}
	fputc('!',fichier);
}

