/**
*\file genetique.h
*Ce fichier decrit le solveur avancé
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdarg.h>
#include <math.h>
#include "Plateau.h"
#include "tuile.h"
#include "insertion.h"
#include "Liste_tuiles.h"
#include "pile.h"
#include "jeu.h"

#ifndef __GENETIQUE_h__
#define __GENETIQUE_h__



typedef struct matrice couche;
struct matrice
{
	float** mat;
	int l;
	int c;

};

typedef struct ind ind;
struct ind
{
	couche value;
	ind* next;
};
typedef ind* population;

typedef struct liste_main liste_main;
struct liste_main
{
	list value;
	liste_main* next;
};
typedef liste_main* mains;

mains create_mains();
void del_mains(mains m);
int mains_is_empty(mains m);
mains mains_vADD(mains m, list x);
void mains_eADD(mains* m, list x);
void print_mains (mains m);
int longer_mains(mains m);

population create_population();
void del_population(population p);
int population_is_empty(population p);
population population_vADD(population p, couche x);
void population_eADD(population* p, couche x);
void print_population (population p);
int longer_population(population p);
int population_elementAt(population p, int index, couche* ret);


couche create_couche(int l, int c);

void reset_couche(couche* X);

void rand_couche(couche* m);

int copie(couche X, couche* Y);

void del_couche(couche* m);

void print_couche(couche* m);

couche feed_forward_step(couche X, couche O);

void mutation_couche_normale(couche* m, float* sigma,float to);

void cross_over(couche pere, couche mere, couche* fils);

int* selection(int* fit, int sz);


void modele1(int taille_grille, int nb_tuile, couche* m1, couche* m2, couche* mf);
couche feed_forward_mdl_1(couche X, couche m1, couche m2, couche mf);
void form_data_mdl_1(couche* X, plateau grille, plateau tuile_cur,list hand);
void save_data_mdl1();

float index_of_max(couche C,int* idx);
void sigmoid(couche* m);
float rand_normale(float mu, float sigma);
int* tri(int* l, int sz);
int e_in(int* l,int sz, int elt);
int* complement(int* l, int sz);
int* extract_rand(int* l, int sz, int new_sz);

void save_population(population p, char* chaine);

#endif

