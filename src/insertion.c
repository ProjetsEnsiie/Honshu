
#include <stdio.h>
#include <stdlib.h>
#include "Plateau.h"
#include "tuile.h"
#include "insertion.h"

int const MAXTUILE = 1000;

void copie_plt(plateau p,plateau *pc){
	int n = p.length;

	int i, j;
	for (i = 0; i < n; ++i)
	{
		for (j = 0; j < n; ++j)
		{
			((pc->values)[i][j]) =  ((p.values)[i][j]);
		}
	}
}

void insert_plateau_tmp( plateau* p, plateau t, int x , int y){

	int n,m;
	n= t.length;
	m= t.width;
	int i;
	int j;

	for (i = 0; i < n; ++i)
	{
		for (j = 0; j < m; ++j)
		{
			(((*p).values)[x+i][y+j]).type = ((t.values)[i][j]).type;
			(((*p).values)[x+i][y+j]).numero = ((t.values)[i][j]).numero;
		}
	}
}

int is_in(int l[], int e){
	if (l[e]==0) return 0;
	return -1;
}

void liste_tuil_plt(plateau p, int l[]){
	int n,e;
	n= p.length;
	int i, j;

	for (i = 0; i < MAXTUILE; ++i)
	{
		l[i] = -1;
	}

	for (i = 0; i < n; ++i)
	{
		for (j = 0; j < n; ++j)
		{
			e = ((p.values)[i][j]).numero;
			if (e!=-1) l[e] = 0;
		}
	}
}

int lcompare(int l1[],int l2[],int e){
    int i;

	for (i = 0; i < MAXTUILE; ++i)
	{
		if (l1[i] != l2[i] && i!=e) return -1;
	}
	return 0;
}

void insert_plateau_start( plateau* p, plateau t){
	int taille = p->length;
	int pos = (taille + (taille%2==0))/2;
	insert_plateau_tmp(p, t, pos-1, pos-1);
}

int insert_plateau( plateau* p, plateau t, int x , int y){

	int n,m,taille_plt;
	n= t.length;
	m= t.width;
	taille_plt = p->length;
	/*test depassement bord*/
	if (x<0 || y<0 || x+n> p->length || y+m > p->length ) return -4;

	/*test oubli de tuile*/
	plateau p_tmp = create_plateau(taille_plt,taille_plt);
	copie_plt(*p,&p_tmp);

	int lp[MAXTUILE];
	liste_tuil_plt(*p,lp);

	insert_plateau_tmp(&p_tmp, t, x, y);
	int lcp[MAXTUILE];
	liste_tuil_plt(p_tmp,lcp);
	if (lcompare(lp,lcp,((t.values)[0][0]).numero) == -1 ) return -1;

	int i;
    int j;
	int b=0;
	int c=0;
	for (i = x; i < x+n; ++i)
	{
		for (j = y; j < y+m; ++j)
		{
			/*test au moins une case 1/2*/
			if ((((*p).values)[i][j]).numero != -1) b=1;
			/*test recouvrement case eau 1/2*/
			if ( ((p->values)[i][j]).type == 'L') c=1;
		}
	}
	/*test au moins une case 2/2*/
	if (b==0) return -2;
	/*test recouvrement case eau 2/2*/
	if (c==1) return -3;



	free_plateau(p_tmp);

	insert_plateau_tmp(p, t, x, y);
	return 0;

}

int est_inserable( plateau* p, plateau t, int x , int y){

	int n,m,taille_plt;
	n= t.length;
	m= t.width;
	taille_plt = p->length;
	/*test depassement bord*/
	if (x<0 || y<0 || x+n> p->length || y+m > p->length ) return -4;

	/*test oubli de tuile*/
	plateau p_tmp = create_plateau(taille_plt,taille_plt);
	copie_plt(*p,&p_tmp);

	int lp[MAXTUILE];
	liste_tuil_plt(*p,lp);

	insert_plateau_tmp(&p_tmp, t, x, y);
	int lcp[MAXTUILE];
	liste_tuil_plt(p_tmp,lcp);
	if (lcompare(lp,lcp,((t.values)[0][0]).numero) == -1 ) return -1;

	int i;
    int j;
	int b=0;
	int c=0;
	for (i = x; i < x+n; ++i)
	{
		for (j = y; j < y+m; ++j)
		{
			/*test au moins une case 1/2*/
			if ((((*p).values)[i][j]).numero != -1) b=1;
			/*test recouvrement case eau 1/2*/
			if ( ((p->values)[i][j]).type == 'L') c=1;
		}
	}
	/*test au moins une case 2/2*/
	if (b==0) return -2;
	/*test recouvrement case eau 2/2*/
	if (c==1) return -3;



	free_plateau(p_tmp);
	return 0;

}






