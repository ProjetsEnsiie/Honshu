/**
*\file insertion.h
*Ce fichier decrit les fonctions d'insertion d'une tuile et de retrait d'une tuile. Il contient 3 fonctions :
*- insert_plateau(p , t, x , y) insert la tuile t sur le plateau p à la position x y et vérifie si il y a des erreurs d'insertion
*- undo(p , t, x , y) retire la tuile t du plateau p
*- insert_plateau_start( p, t) insert la prmière tuile \t au centre du plateau \a p
*/


#ifndef __INSERTION_h__
#define __INSERTION_h__

#include <stdio.h>
#include <stdlib.h>
#include "Plateau.h"
#include "tuile.h"


/**
* \brief Insert la tuile \a t sur le plateau \a p à la position \a x \a y après avoir fait tous les tests d'insertion.
* \param p Un pointeur vers un plateau.
* \param t Une tuile.
* \param x Un entier.
* \param y un entier.
* \return 0 si l'insertion est possible, -1 si une tuile serait totalement recouverte, -2 si on ne recouvre pas au moins une case de tuile du plateau, -3 si on recouvre une case d'eau et -4 si on sort de la grille.
*/
int insert_plateau( plateau* p, plateau t, int x , int y);


void copie_plt(plateau p,plateau *pc);

/**
* \brief Insert la première tuile \a t sur le plateau \a p
* \param p Un pointeur vers un plateau.
* \param t Une tuile.
*/
void insert_plateau_start( plateau* p, plateau t);

int est_inserable( plateau* p, plateau t, int x , int y);

#endif
