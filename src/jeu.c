#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "Plateau.h"
#include "tuile.h"
#include "insertion.h"
#include "Liste_tuiles.h"
#include "loadAndSave.h"
#include "pile.h"

#include "jeu.h"


int poser_tuile(list* tlist, int id, plateau* grille, int i, int j,Stack* tpile){
	plateau t;
	int midx,k,err;
	int taille_main = longer_list(*tlist);
	quadruplet trpl;

	midx = -1;
	for (k=0;k<taille_main;k++){
		list_elementAt(*tlist, k, &t);
		if ( ((t.values)[0][0]).numero == id ) midx = k;
	}
	if (midx == -1) return -5;
	list_elementAt(*tlist, midx, &t);

    plateau tuile_test = load_tuile_n("tuiles.txt", t.values[0][0].numero);

    if(t.length == tuile_test.length && t.length == 3)
    {
        if (equal_tuile(t, tuile_test))
        {
            trpl.rotation = 0;
        }
        else
        {
            trpl.rotation = 2;
        }
    }
    else
    {
        rotation_gauche(&tuile_test);
        if(equal_tuile(t, tuile_test))
        {
            trpl.rotation = 1;
        }
        else
        {
            trpl.rotation = 3;
        }
    }
    free_plateau(tuile_test);

	err = insert_plateau(grille, t, i, j);

	if (err != 0) return err;
	rm_element(tlist, midx);


	trpl.i = i;
	trpl.j = j;
	trpl.id = id;

	push(tpile, trpl);
	return err;

}

