/**
*\file jeu.h
*Ce fichier decrit les fonctions nécessaire au déroulement d'une partie. Il contient 1 fonctions :
*- poser_tuile(tlist, id, grille, i, j, tpile) Pose une tuile demandée sur une grille et stocke le mouvement dans une pile
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "Plateau.h"
#include "tuile.h"
#include "insertion.h"
#include "Liste_tuiles.h"
#include "loadAndSave.h"
#include "pile.h"


#ifndef __JEU_h__
#define __JEU_h__

/**
* \brief Pose la tuile numéro \a id de la main \a tlist sur la grille \a grille à la position ( \a i , \a j ),p uis sotocke les information du mouvement dans la pile \ tpile .
* \param tlist Une liste de tuile.
* \param id Le numéro de la tuile à poser.
* \param grille Un pointeur vers la grille sur laquelle on souhaite poser la tuile.
* \param i La position en abscisse.
* \param j La position en ordonnée.
* \param tpille Un pointeur vers la pile où l'on veut ajouter les informations de mouvement.
* \return l'erreur classique de position d'une tuile.
*/
int poser_tuile(list* tlist, int id, plateau* grille, int i, int j, Stack* tpile);



#endif

