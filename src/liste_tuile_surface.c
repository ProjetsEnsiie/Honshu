#include <stdlib.h>

#include <stdio.h>

#include <SDL/SDL.h>

#include "liste_tuile_surface.h"


typedef tsrf* list_tsrf;

list_tsrf create_list_tsrf()
{
    return NULL;
}

int list_tsrf_is_empty(list_tsrf l)
{
    return (l==NULL);
}

list_tsrf list_tsrf_vADD(list_tsrf l, SDL_Surface* s,int x, int y)
{
    list_tsrf newcell = (list_tsrf)malloc(sizeof(tsrf));
    newcell->aff = 1;
    newcell->value=s;
    (newcell->position).x = x;
    (newcell->position).y = y;
    newcell->next=l;
    return newcell;
}

void list_tsrf_eADD(list_tsrf* pl, SDL_Surface* s,int x, int y)
{
    *pl = list_tsrf_vADD(*pl, s,x,y);
}

void print_list_tsrf (list_tsrf l,SDL_Surface* ecran)
{
    list_tsrf currentlist=l;
    while (currentlist!=NULL)
    {
        SDL_BlitSurface(currentlist->value, NULL, ecran, &(currentlist->position));
        currentlist=currentlist->next;
    }
}

int longer_list_tsrf(list_tsrf l)
{
    int i=0;
    list_tsrf currentlist_tsrf=l;
    while (currentlist_tsrf!=NULL)
    {
        i++;
        currentlist_tsrf=currentlist_tsrf->next;
    }
    return i;
}

void free_list_tsrfe(list_tsrf l){
    while (!list_tsrf_is_empty(l)){
        SDL_FreeSurface(l->value);

        list_tsrf tmp = l;
        l = l->next;
        free(tmp);
    }
    free(l);
}


int surface_id_at_pos(list_tsrf l, int x, int y,int* id,int long_t, int larg_t){
    list_tsrf currentlist=l;
    int i = 0;
    while (currentlist!=NULL)
    {
        if(x > currentlist->position.x  && x < (currentlist->position.x + larg_t) && y > currentlist->position.y && y < (currentlist->position.y + long_t))
            {
                *id = i;
                return 1;
            }
        currentlist=currentlist->next;
        i++;
    }
    return 0;
}

/*int surface_at_id(list_tsrf l, int id,list_tsrf* lSurf ){
    list_tsrf currentlist=l;
    int i = 0;
    while (currentlist!=NULL)
    {
        if(i==id)
            {
                *lSurf = currentlist;
                return 1;
            }
        currentlist=currentlist->next;
        i++;
    }
    return 0;
}*/
int surface_at_id(list_tsrf l, int id, SDL_Surface** ret)
{
    list_tsrf current=l;
    while (current!=NULL && id>0)
    {
        id--;
        current=current->next;
    }
    if (current==NULL)
    {
        return 0;
    }
    else
    {
        *ret=current->value;
        return 1;
    }
}

int set_surface_at_id(list_tsrf* l, int id, SDL_Surface* new ){
    list_tsrf currentlist= *l;
    list_tsrf newList = create_list_tsrf();
    int i = 1;
    if (id == 0){
        list_tsrf_eADD(&newList, new, (currentlist->position).x, (currentlist->position).y );
        newList->next = (*l)->next;
        SDL_FreeSurface(currentlist->value);
        *l = newList;
        return 1;
    }
    list_tsrf predlist = currentlist;
    currentlist = currentlist->next;
    while (currentlist!=NULL)
    {
        if (i == longer_list_tsrf(*l)-1){
            list_tsrf_eADD(&newList, new, (currentlist->position).x, (currentlist->position).y);
            newList->next = NULL;
            SDL_FreeSurface(currentlist->value);
            predlist->next = newList;
            return 1;
        }
        if(i==id)
            {
                list_tsrf_eADD(&newList, new, (currentlist->position).x, (currentlist->position).y);
                newList->next = currentlist->next;
                SDL_FreeSurface(currentlist->value);
                predlist->next = newList;
                return 1;
            }
        predlist = currentlist;
        currentlist = currentlist->next;
        i++;
    }
    return 0;
}


int set_pos_at_id(list_tsrf* l, int id,int x, int y){
    list_tsrf currentlist= *l;
    int i = id;
    
    if (id > longer_list_tsrf(*l)) return 0;
    while (i> 0){
        currentlist = currentlist->next;
        i--;
    }
    

    currentlist->position.x = x;
    currentlist->position.y = y;
    

    return 1;
}


int rm_sfr_element(list_tsrf* pl, int idx)
{
    if(idx==0)
    {
       *pl = (*pl)->next;
        return 1;
    }
    list_tsrf final = *pl;
    list_tsrf previous;
    list_tsrf current=final;
    while (current!=NULL && idx>0)
    {
        idx--;
        previous=current;
        current=current->next;
    }
    if (current==NULL)
    {
        return 0;
    }
    else
    {
        previous->next=(current->next);
        SDL_FreeSurface(current->value);
        free(current);
        return 1;
    }
}

void list_tsrf_eADD_END(list_tsrf* l, SDL_Surface* s,int x, int y)
{
    list_tsrf newcell = (list_tsrf)malloc(sizeof(tsrf));
    newcell->aff = 1;
    newcell->value=s;
    (newcell->position).x = x;
    (newcell->position).y = y;
    newcell->next = create_list_tsrf();

    list_tsrf cur = *l;
    while (cur->next!=NULL){
        cur = cur->next;
    }
    cur->next = newcell;
}
