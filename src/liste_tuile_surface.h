#include <stdlib.h>

#include <stdio.h>

#include <SDL/SDL.h>



#ifndef __LISTE_TUILE_SURFACE_h__
#define __LISTE_TUILE_SURFACE_h__

typedef struct tsrf tsrf;
struct tsrf
{
    SDL_Surface* value;
    SDL_Rect position;
    int aff;
    tsrf* next;
};

typedef tsrf* list_tsrf;


list_tsrf create_list_tsrf();


int list_tsrf_is_empty(list_tsrf l);


list_tsrf list_tsrf_vADD(list_tsrf l, SDL_Surface* s,int x, int y);


void list_tsrf_eADD(list_tsrf* pl, SDL_Surface* s,int x, int y);


void print_list_tsrf (list_tsrf l,SDL_Surface* ecran);


int longer_list_tsrf(list_tsrf l);

void free_list_tsrfe(list_tsrf l);

int surface_id_at_pos(list_tsrf l, int x, int y,int* id,int long_t, int larg_t);

int surface_at_id(list_tsrf l, int id, SDL_Surface** ret);

int set_surface_at_id(list_tsrf* l, int id,SDL_Surface* new );

int set_pos_at_id(list_tsrf* l, int id,int x, int y);

int rm_sfr_element(list_tsrf* pl, int idx);

void list_tsrf_eADD_END(list_tsrf* l, SDL_Surface* s,int x, int y);

#endif
