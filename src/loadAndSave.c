#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "Plateau.h"
#include "tuile.h"
#include "insertion.h"
#include "Liste_tuiles.h"
#include "pile.h"

#include "loadAndSave.h"

plateau load_tuile_n(const char* chaine ,int n){

	FILE* fichier = NULL;
	fichier = fopen(chaine,"r");
	char c[20];
	char tuile_chaine[20] = "";
	int i = -1;
	int count = -1;

	fgets(c,20,fichier);

	while(fgets(c,20,fichier) != NULL && i !=n ){

		if (strlen(c)<=3){
			char c_f[20] = "";
			unsigned int j;
			for (j = 0; j < strlen(c); ++j)
			{
				if (c[j]!='\n') {
					strcat(c_f,c);
				}
			}
			i = atoi(c);
			count++;
		}
	}


	if (count != n ) {
		printf("error : %d not in %s\n",n,chaine );
		fclose(fichier);
		return create_tuile("      ",-1);
	}

	for (i= 0; i < 3; ++i)
	{
		char c_f[20] = "";
		int k = 0;
		unsigned int j;
		for (j = 0; j < strlen(c); ++j)
		{
			if (c[j]!='\n' && c[j] != ' ') {
				c_f[k] = c[j];
				k++;
			}
		}

		strcat(tuile_chaine,c_f);
		fgets(c,20,fichier);
	}

	fclose(fichier);

	return create_tuile(tuile_chaine,n);
}

plateau load_plateau(int taille, const char* chaine, int n){
	plateau g = create_plateau(taille,taille);
	plateau t = load_tuile_n(chaine,n);
	insert_plateau_start(&g,t);
	free_plateau(t);
	return g;
}


int readTo(FILE* fichier, char end){

	int c2 = 0;
	char c1[20] = "";
	int i=0;
	while(c2 != (int) end){
		c2 = fgetc(fichier);
		c1[i] = c2;
		i++;
	}

	return atoi(c1);
}


int load_partie(const char* chaine_t,const char* chaine_p, plateau* g, list* liste_t,Stack* pile){

	FILE* fichier = NULL;
	if( access( chaine_p, F_OK ) == -1 ) {
    return -1;
	}
	fichier = fopen(chaine_p,"r");

	int taille_plateau = -1;
	int taille_main = -1;

	taille_plateau = readTo(fichier,' ');
	taille_main = readTo(fichier,'\n');

	int i,t;
	for (i = 0; i < taille_main-1; i++)
	{
		t = readTo(fichier,' ');
		list_eADD(liste_t, load_tuile_n(chaine_t, t));

	}
	t = readTo(fichier,'\n');
	list_eADD(liste_t, load_tuile_n(chaine_t, t));
	t = readTo(fichier,'\0');

	int pos = (taille_plateau + (taille_plateau%2==0))/2;
	quadruplet trp = create_quadruplet(t,pos-1,pos-1, 0);
	push(pile,trp);

	*g = load_plateau(taille_plateau,chaine_t,t);

	fclose(fichier);
	return 0;
}

int file_len(const char* chaine){
	FILE* fichier = fopen(chaine,"r");
	int i = -1;
	char c;
	do {
		c =fgetc(fichier);
		i++;
	} while (c != EOF);
	fclose(fichier);
	return i;
}
int get_tuile_id(plateau t){
	return ((t.values)[0][0]).numero;
}

void save_tuile(const char* chaine, plateau t){
	int f_taille = file_len(chaine);
	FILE* fichier = NULL;
	fichier = fopen(chaine, "r+");
	char* c_tuile = get_string_tuile(t);
	char c[3] = "";
	int n;

	fseek(fichier, 0, SEEK_SET);
	if(f_taille==0){fputs("0",fichier);}

	n = readTo(fichier,'\n') +1 ;

	sprintf(c,"%d",n);
	fputs(c,fichier);

	fseek(fichier,0,SEEK_END);
	fputc('\n',fichier);

	sprintf(c,"%d",n-1);
	fputs(c,fichier);
	fputc('\n',fichier);


	int i;
	for (i = 0; i < 6; ++i)
	{
		fputc(c_tuile[i],fichier);
		if ((i+1)%2!=0){
			fputc(' ',fichier);
		}
		else if ( i!=5){
				fputc('\n',fichier);
		}
	}
	fclose(fichier);
}




void save_partie(const char* chaine, int taille, int nb_tuiles, list liste_t, plateau t_strt){
	FILE* fichier = fopen(chaine,"w+");
	char c[10] = "";

	sprintf(c,"%d",taille);
	fputs(c,fichier);

	fputc(' ',fichier);
	sprintf(c,"%d",nb_tuiles);
	fputs(c,fichier);

	fputc('\n',fichier);
	int i =1;
	list l = liste_t;
	while(!list_is_empty(l))
	{

		/*sprintf(c,"%d",(((liste_t[i]).values)[0][0]).numero);*/

		sprintf(c,"%d",(((l->value).values)[0][0]).numero);

		fputs(c,fichier);
		if (i!=nb_tuiles){
			fputc(' ',fichier);
		}
		i++;
		l = l->next;
	}
	fputc('\n',fichier);
	sprintf(c,"%d",get_tuile_id(t_strt));
	fputs(c,fichier);
}

void inverse_pile(Stack* pile1, Stack* pile2){
	while(pile1->count != 0){
		quadruplet t = pop(pile1);
		push(pile2, t);
	}
}

void reconstruction(plateau* p,Stack* s){
	int i;

	Stack pileinv;
	Init_Stack(&pileinv);
	inverse_pile(s, &pileinv);

	quadruplet t = pop(&pileinv);
	push(s, t);
	plateau tui = load_tuile_n("tuiles.txt" ,t.id);
	insert_plateau_start( p, tui);
	free_plateau(tui);


	while(pileinv.count != 0){
		t = pop(&pileinv);
		push(s, t);
		plateau tui = load_tuile_n("tuiles.txt" ,t.id);

		for (i=0;i<t.rotation;i++){
			rotation_gauche(&tui);
		}
		insert_plateau( p, tui, t.i , t.j);
		free_plateau(tui);
	}
}

int undo( plateau* p,int taille, list* hand, Stack* pile){
	if (pile->count <= 1 ) return -1;
	quadruplet t = pop(pile);
	plateau tui = load_tuile_n("tuiles.txt" ,t.id);
	list_eADD(hand, tui);  /*  ATTENTION CASSÉ          */
	int i,j;
	for (i = 0; i < taille; ++i)
	{
		for (j = 0; j < taille; ++j)
		{
			(p->values)[i][j]  = create_terrain(' ',-1);
		}
	}
	reconstruction(p,pile);
	return 0;
}
