/**
*\file loadAndSave.h
*Ce fichier decrit les fonctions de chargement et de sauvgarde de débuts de partie. Il contient 5 fonctions :
*- load_tuile_n(chaine , n) crée la tuile n depuis le fichier de tuiles chaine
*- load_plateau(taille,chaine,n) crée un plateau de taille taille et place la tuile n au centre
*- load_partie( chaine_t, chaine_p ,  g,  liste_t) crée une partie en chargeant les donnée de la partie chaine et en affectant à g sa plateau et à liste_t la liste des tuiles
*- save_tuile(chaine,  t) écrit dans un fichier de tuiles une tuile t
*- save_partie(chaine,  taille,  nb_tuiles,  liste_t,  t_strt) écrit dans un fichier une partie avec les parametre corespondant
*- undo( p,taille, hand,pile) Retire la dernère posée tuile d'un plateau
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Plateau.h"
#include "tuile.h"
#include "insertion.h"
#include "Liste_tuiles.h"
#include "pile.h"



#ifndef __LOADANDSAVE_h__
#define __LOADANDSAVE_h__


/**
* \brief Crée la tuile \a n depuis le fichier de tuiles \a chaine.
* \attention Le fichier \a chaine doit exister.
* \param chaine Une chaine de carcatère contenant l'adresse d'un fichier.
* \param n L'indice d'une tuile.
* \return La tuile d'indice \a n du fichier \a chaine.
*/
plateau load_tuile_n(const char* chaine ,int n);

/**
* \brief Crée un plateau de taille \a taille et place la tuile \a n au centre.
* \attention Le fichier \a chaine doit exister.
* \param taille Entier positif.
* \param chaine Une chaine de carcatère contenant l'adresse d'un fichier.
* \param n L'indice d'une tuile.
* \return un plateau de taille \a taille contenant la tuile \a n du fichier \a chaine.
*/
plateau load_plateau(int taille, const char* chaine, int n);

/**
* \brief Crée une partie en chargeant les donnée de la partie \a chaine et en affectant à \a g sa plateau et à \a liste_t la liste des tuiles.
* \attention Les fichiers \a chaine_t \a chaine_p doivent exister.
* \param chaine_t Une chaine de carcatère contenant l'adresse d'un fichier de tuile.
* \param chaine_p Une chaine de carcatère contenant l'adresse d'un fichier de partie.
* \param g Un pointeur vers un plateau.
* \param liste_t Une liste de tuile.
* \param pile Une pile de mouvements.
* \return 0 si le fichier existe -1 sinon
*/
int load_partie(const char* chaine_t,const char* chaine_p, plateau* g, list* liste_t,Stack* pile);

/**
* \brief Ecrit dans un fichier \a chaine de tuiles une tuile \a t
* \param chaine Une chaine de carcatère contenant l'adresse d'un fichier.
* \param t Une tuile.
*/
void save_tuile(const char* chaine, plateau t);

/**
* \brief Ecrit dans un fichier \a chaine une partie avec les parametre corespondant
* \param chaine Une chaine de carcatère contenant l'adresse d'un fichier.
* \param taille Un entier positif : taille de le plateau.
* \param nb_tuiles Un entier positif : nombre de tuiles.
* \param liste_t Une liste de tuile.
* \param t_strt Une tuile : à placer au début de la partie.
*/
void save_partie(const char* chaine, int taille, int nb_tuiles, list liste_t, plateau t_strt);


/**
* \brief Retire la dernière tuile posée du plateau \a p.
* \param p Un pointeur vers un plateau.
* \param taille Un entier correspondant à la taille du plateau.
* \param hand Une liste de tuile.
* \param pile Une pile de mouvements.
*/
int undo( plateau* p,int taille, list* hand, Stack* pile);

int readTo(FILE* fichier, char end);

#endif
