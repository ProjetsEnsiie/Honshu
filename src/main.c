#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdarg.h>
#include <math.h>

#include "Plateau.h"
#include "tuile.h"
#include "insertion.h"
#include "Liste_tuiles.h"

#include "loadAndSave.h"
#include "pile.h"
#include "jeu.h"

#include "solveur.h"
#include "genetique.h"
#include "solveurGenetique.h"



int main(int argc, char* argv[]) {

    if (argc>1 ) {
        if ( strcmp(argv[1],"sg") ==0 ){
            printf("%d\n", train(argc, argv+1)) ;
            return 1;
        }
    }

    plateau g;
    list liste = create_list();
    char charge[100];
    char mode= '0';
    int b = 0;
    Stack tpile;
    Init_Stack(&tpile);

    /* TEST HONSHU_OPT */
    /*load_partie("tuiles.txt", "partie3.txt", &g, &liste, &tpile);
    int n = g.length;
    list T = create_list();
    int moyenne = ((n + (n%2 == 0))/2);
    int id = g.values[moyenne][moyenne].numero;
    plateau tuile = load_tuile_n("tuiles.txt", id);
    list_eADD(&T, tuile);

    int x = Honshu_Opt(g, n, T, liste);
    printf("%d\n\n\n", x);*/

    while(b==0){
        srand(time(NULL));
        printf("Voulez-vous charger depuis un fichier (f) ou charger aleatoirement (a) ?\n");
        scanf(" %c",&mode);
        if (mode=='f'){
            printf("liste de fichier :\n");
            system("ls partie*\n");
            scanf(" %s",charge);
            if (load_partie("tuiles.txt", charge, &g, &liste,&tpile)==0){
              b=1;
            }
            else{
              printf("%s\n","Nom de fichier invalide" );
            }
        }
        else if (mode=='a'){
            int nb,taille;
            int min = 10;
            int max = 20;
            taille = rand()/(double)RAND_MAX  * (max-min) + min;
            min = 5;
            max = 1.5*taille;
            max=(max>15)?15:max;

            nb =  rand()/(double)RAND_MAX  * (max-min) + min;
            printf("taille de la grille : %d\n",taille);
            printf("nombre de tuiles : %d\n",nb);
            int i;
            g = create_plateau(taille, taille);
            insert_plateau_start( &g, rand_tuile(0));
            for (i = 0; i < nb; ++i)
            {
                list_eADD(&liste, rand_tuile(taille -(i+1)));
            }
            b=1;
        }
        else{
          printf("Choix non reconnu, veuillez réessayer\n");
        }


    }


    while (!list_is_empty(liste))
    {

        print_plateau(g);
        printf("\n");
        print_list(liste);

        printf("Voulez-vous poser une tuile ? entrez o ou n\n");
        char lue;
        scanf(" %c", &lue);
        if (lue == 'o')
        {
            printf("Quelle tuile ? entrez id\n");
            int id;
            scanf("%d", &id);
            printf("Quelle position ? entrez i puis j\n");
            int i;
            int j;
            scanf("%d", &i);
            scanf("%d", &j);

            poser_tuile(&liste, id, &g, i, j, &tpile);
        }

        else if (lue == 'n')
        {
            char reponse;
            printf("Voulez-vous effectuer un undo ? entrez o ou n\n");
            scanf(" %c", &reponse);
            if (reponse == 'o'){
                printf("undo\n");
                undo( &g,g.length, &liste, &tpile);
            }
            else if (reponse == 'n'){
                printf("Voulez-vous effectuer une rotation ? entrez o ou n\n");
                scanf(" %c", &reponse);

                if (reponse == 'o')
                {
                    printf("Quelle tuile ? entrez id\n");
                    int id = 0;
                    scanf("%d", &id);

                    plateau* tuile_rot = NULL;
                    list_element_idbis(liste, id, &tuile_rot);
                    print_plateau(*tuile_rot);
                    printf("Quelle type de rotation ? entrez g ou d\n");



                    char rotation;
                    scanf(" %c", &rotation);

                    if (rotation == 'g')
                    {
                        rotation_gauche(tuile_rot);
                        print_plateau(*tuile_rot);
                    }
                    else if (rotation == 'd')
                    {
                        rotation_droite(tuile_rot);
                        print_plateau(*tuile_rot);
                    }

                }
                else if (reponse == 'n') {

                    char regle;
                    printf("Voulez-vous revoir les règles ? entrez o ou n\n");
                    scanf(" %c", &regle);
                    if (regle == 'o') {

                    printf("\n\n\nLes règles du jeu sont les suivantes :\n\n");
                    printf("Une tuile ne peut être totalement recouverte à aucun moment de la partie.\n");
                    printf("Toute nouvelle tuile posée doit recouvrir au moins une case de l’une des tuiles\n");
                    printf("précédemment posée, la précédente case est alors oubliée.\n");
                    printf("Aucune case Lac ne peut être recouverte.\n");
                    printf("Le joueur peut faire des rotations pour placer les tuiles dans le sens qu’il souhaite.\n\n\n");

                    printf("Les règles pour marquer des points sont :\n\n");
                    printf("1. On regarde le plus grand village (plus grand bloc de cases Ville contigües).\n");
                    printf("Le joueur marque (nombre de case ville de ce village) points.\n");
                    printf("2. On compte les Forets. Le joueur marque 2x (nombre de cases forêt) points.\n");
                    printf("3. On regarde tout les Lacs. Le joueur marque 3x (nombre de cases Lac du lac -1) points.\n");
                    printf("4. On compte les Ressources. On alloue une Ressource par Usine tant qu’il reste des cases Ressource et des case Usine.\n");
                    printf("Chaque Ressource alloué à une Usine rapporte 4points. Une Usine ne peut traiter qu’une Ressource et une Ressource ne peut être allouée qu’à une Usine.\n");
                    printf("Une Usine peut traiter une ressource d’une case qui ne lui est pas contigüe.\n\n");



                    }

                }
            }
            else
            {
                printf("erreur\n");
            }

        }



    }
    print_plateau(g);
    printf("\nFélicitation, vous avez fini.\n");
    printf("Revenez vite jouer avec nous !\n");

    return 0;
}
