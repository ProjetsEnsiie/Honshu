#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "Plateau.h"
#include "tuile.h"
#include "insertion.h"
#include "Liste_tuiles.h"

#include "pile.h"


quadruplet create_quadruplet (int id, int i, int j, int rotation)
{
    quadruplet tri;
    tri.id = id;
    tri.i = i;
    tri.j = j;
    tri.rotation = rotation;
    return tri;
}

void Init_Stack(Stack* ps)
{
    (*ps).count=0;
}

int Stack_empty(Stack s)
{
    return (s.count==0);
}

int push(Stack* ps, quadruplet x)
{
    if(ps->count==50)
    {
        return 0;
    }
    else
    {
        ps->values[ps->count]=x;
        ps->count++;
        return 1;
    }
}

quadruplet pop(Stack* ps)
{
    quadruplet y = create_quadruplet(0, 0, 0, 0);

    if(Stack_empty(*ps)==1)
    {
        return create_quadruplet(0, 0, 0, 0);
    }
    else
    {
        y=ps->values[ps->count-1];
        ps->values[ps->count-1] = create_quadruplet(0, 0, 0, 0);
        ps->count--;
        return y;
    }
}

void print_Stack(Stack s)
{
    int i=0;
    for(i=0; i<(s.count); i++)
    {
        printf("%d, (%d, %d), %d\n", s.values[s.count - i - 1].id, s.values[s.count - i - 1].i, s.values[s.count - i - 1].j, s.values[s.count - i - 1].rotation);
    }
    printf("\n");
}

