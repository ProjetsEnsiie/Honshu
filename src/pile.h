#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "Plateau.h"
#include "tuile.h"
#include "insertion.h"
#include "Liste_tuiles.h"

#ifndef PILE_H
#define PILE_H

typedef struct quadruplet quadruplet;
struct quadruplet
{
    int id;
    int i;
    int j;
    int rotation;
};

typedef struct Stack Stack;
struct Stack
{
    int count;
    quadruplet values[50];
};

quadruplet create_quadruplet (int id, int i, int j, int rotation);
void Init_Stack(Stack* ps);

int Stack_empty(Stack s);

int push(Stack* ps, quadruplet x);

quadruplet pop(Stack* ps);

void print_Stack(Stack s);








#endif
