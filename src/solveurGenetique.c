#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdarg.h>
#include <math.h>
#include <sys/types.h>

#include <unistd.h>
#include "solveurGenetique.h"

#include "Plateau.h"
#include "tuile.h"
#include "insertion.h"
#include "Liste_tuiles.h"

#include "loadAndSave.h"
#include "pile.h"
#include "jeu.h"
#include "compte_point.h"

#include "genetique.h"




int init_partie(char* typePartie, plateau* g, list* liste, Stack* tpile, int* taille_grille, int* taille_main){
	if (strcmp(typePartie,"a")!=0){
        if (load_partie("tuiles.txt", typePartie, g, liste,tpile)==0){
          *taille_grille = g->length;
          *taille_main = longer_list(*liste);
        }
        else{
          printf("%s\n","Nom de fichier invalide" );
          return -1;
        }
    }
    else if (strcmp(typePartie,"a")==0){
        int i;
        *g = create_plateau(*taille_grille, *taille_grille);
        insert_plateau_start( g, rand_tuile(0));
        for (i = 0; i < *taille_main; ++i)
        {
            list_eADD(liste, rand_tuile(*taille_main -(i+1)));
        }
    }
    return 0;
}

void jeu( int taille_main, int taille_grille, plateau* g,list *liste, Stack* tpile, couche* m1, couche* m2, couche* mf){
	list l_cur  = *liste;
	int i =0;
	int taille_couche = taille_main*6 + taille_grille*taille_grille + 6 + 1;
	couche X,R;
	X = create_couche(1, taille_couche);
	while (!list_is_empty(l_cur) && i <= 100){ /*	A REVOIR POUR LE NOMBRE D ITERATION MAX	*/
		/*Calcul tache à accomplir*/
		form_data_mdl_1(&X, *g, l_cur->value, *liste);
		R = feed_forward_mdl_1(X, *m1, *m2, *mf);

		int action;
		float val_act= index_of_max(R,&action);
		val_act += 1;
		
		del_couche(&R);
		/*Application de la tache*/
		
		int id = ((l_cur->value).values[0][0]).numero;
		
		if (action < taille_grille*taille_grille){ /*Position*/
			int x,y;
			y = action/taille_grille;
			x = action%taille_grille;
			poser_tuile(liste,  0, g,  x,  y, tpile);
		}
		else if (action == R.c -4) {   /*Rotation droite*/
			plateau* tuile_rot = NULL;
            list_element_idbis(*liste, id , &tuile_rot);
            rotation_droite(tuile_rot);
		}
		else if (action == R.c- 3 ) {   /*Rotation gauche*/
			plateau* tuile_rot = NULL;
            list_element_idbis(*liste, id , &tuile_rot);
            rotation_gauche(tuile_rot);
		}
		if (l_cur->next == NULL) l_cur = *liste;
		else l_cur = l_cur->next;
		i++;

		



	}
	del_couche(&X);

}

float fmax(float a, float b){
	if (a<b) return b;
	return a;
}

int train(int argc, char* argv[]){

	int SIZE  =  12*4;



	srand(time(NULL));
	int n; /*nombre de générations*/
	char* typePartie;
	plateau g;
    list listetmp = create_list();
	Stack tpile;
    Init_Stack(&tpile);
    int taille_grille;
    int taille_main;
    float to;
    float sigma;

    float* sigmas = (float*) malloc(SIZE*sizeof(float));
    int* scores = (int*) malloc(SIZE*sizeof(int));
    list ensemble_plt  =  create_list();
    mains ensemble_main = create_mains();


	if (argc == 2) {
		sigma = 2.;
		to = 1.;
		n=10;
		typePartie = "a";
		taille_grille = 9;
		taille_main = 12;
		
	}
	else if (argc == 3) {
		sigma = (float)  atof(argv[1]);
		to = 1.;
		n=10;
		typePartie="a";
		taille_grille = 9;
		taille_main = 12;
		
	}
	else if (argc == 4) {
		sigma = atof(argv[1]);
		to = atof(argv[2]);
		n=10;
		typePartie="a";
		taille_grille = 9;
		taille_main = 12;
	}
	else if (argc == 5) {
		sigma =(float)  atof(argv[1]);
		to =(float)  atof(argv[2]);
		n = atoi(argv[3]);
		typePartie="a";
		taille_grille = 9;
		taille_main = 12;
	}
	else if (argc == 6){
		sigma =(float)  atof(argv[1]);
		to =(float)  atof(argv[2]);
		n = atoi(argv[3]);
		typePartie = argv[4];
		taille_grille = 9;
		taille_main = 12;
		
	}
	else if(argc == 7){
		sigma =(float)  atof(argv[1]);
		to =(float)  atof(argv[2]);
		n = atoi(argv[3]);
		typePartie = argv[4];
		taille_grille = atoi(argv[5]);
		taille_main = 12;

	}
	else{
		
		sigma =(float)  atof(argv[1]);
		to =(float)  atof(argv[2]);
		n = atoi(argv[3]);
		typePartie = argv[4];
		taille_grille = atoi(argv[5]);
		taille_main = atoi(argv[6]);
	}

	/*intitialisation de la partie*/
	
	if ( init_partie(typePartie, &g, &listetmp, &tpile, &taille_grille, &taille_main) == -1) return -1;
	free_liste(listetmp);
	
	
    /*Céation d'un population*/
	population M1, M2, Mf;
	
	M1 = create_population();
	M2 = create_population();
	Mf = create_population();
	couche m1, m2, mf;
	int u;
	for (u = 0; u < SIZE; ++u)
	{
		modele1(taille_grille, taille_main, &m1, &m2, &mf);
		population_eADD(&M1, m1);
		population_eADD(&M2, m2);
		population_eADD(&Mf, mf);
	}

	
    /*Entrainement*/

    for (u = 0; u < SIZE; ++u)
    {
    	sigmas[u] = sigma;
    }
    
    int t;
    plateau gfin = copie_plateau(g);
    free_plateau(g);
    for (t = 0; t < n; ++t)
    {	
    	ensemble_plt  =  create_list();
   		ensemble_main = create_mains();
   		listetmp = create_list();
   		
    	init_partie(typePartie, &g, &listetmp, &tpile, &taille_grille, &taille_main);
    	for (u = 0; u < SIZE; ++u){
    		list_eADD(&ensemble_plt,copie_plateau(g));
    		mains_eADD(&ensemble_main, copie_list(listetmp));
    	}
    	
    	/*Jeu*/
    	population curM1, curM2, curMf;
    	list curPlt = ensemble_plt;
    	mains curMain = ensemble_main;
    	curM1 = M1;
    	curM2 = M2;
    	curMf = Mf;


    	

    	for (u = 0; u < SIZE; ++u)
    	{	
    		
    		jeu(taille_main,taille_grille, &(curPlt->value), &(curMain->value), &tpile, &(curM1->value), &(curM2->value), &(curMf->value));
    		
    		curM1 = curM1->next;
    		curM2 = curM2->next;
    		curMf = curMf->next;
    		curPlt = curPlt->next;
    		curMain = curMain->next;
    	}
    	

    	/*Séléction*/
    	curPlt = ensemble_plt;
    	for (u = 0; u < SIZE; ++u){
    		scores[u] = points(curPlt->value);
    		curPlt = curPlt->next;
    	}
    	int * idx_parents = selection(scores, SIZE);
    	int * idx_enfants = complement(idx_parents, SIZE/2);
    	
    	

    	/*Evolution*/
    	
    	/*	copie*/
    	for (u = 0; u < SIZE/2; ++u)
    	{
    		couche pere1,fils1;
    		couche pere2,fils2;
    		couche peref,filsf;

    		population_elementAt(M1, idx_parents[u], &pere1);
    		population_elementAt(M1, idx_enfants[u], &fils1);

    		population_elementAt(M2, idx_parents[u], &pere2);
    		population_elementAt(M2, idx_enfants[u], &fils2);

    		population_elementAt(Mf, idx_parents[u], &peref);
    		population_elementAt(Mf, idx_enfants[u], &filsf);

    		copie(pere1, &fils1);
    		copie(pere2, &fils2);
    		copie(peref, &filsf);

    		sigmas[idx_enfants[u]] = sigmas[idx_parents[u]];

    	}

    	/*	crossing over*/
    	int co = 2*(rand()%(SIZE/4));
    	int * idx_parents_co = extract_rand(idx_parents,SIZE/2,co); 
    	
    	for (u = 0; u < co/2; ++u)
    	{
    		couche pere1,mere1,fils1;
    		couche pere2,mere2,fils2;
    		couche peref,meref,filsf;

    		population_elementAt(M1, idx_parents_co[u], &pere1);
    		population_elementAt(M1, idx_parents_co[u+co/2], &mere1);
    		population_elementAt(M1, idx_enfants[u], &fils1);

    		population_elementAt(M2, idx_parents_co[u], &pere2);
    		population_elementAt(M2, idx_parents_co[u+co/2], &mere2);
    		population_elementAt(M2, idx_enfants[u], &fils2);

    		population_elementAt(Mf, idx_parents_co[u], &peref);
    		population_elementAt(Mf, idx_parents_co[u+co/2], &meref);
    		population_elementAt(Mf, idx_enfants[u], &filsf);

    		cross_over(pere1, mere1, &fils1);
    		cross_over(pere2, mere2, &fils2);
    		cross_over(peref, meref, &filsf);
    	}

    	/*	mutations*/

    	for (u = 0; u < SIZE/2; ++u)
    	{
    		couche fils1;
    		couche fils2;
    		couche filsf;

    		population_elementAt(M1, idx_enfants[u], &fils1);

    		population_elementAt(M2, idx_enfants[u], &fils2);

    		population_elementAt(Mf, idx_enfants[u], &filsf);

    		mutation_couche_normale(&fils1, &(sigmas[idx_enfants[u]]),to);
    		mutation_couche_normale(&fils2, &(sigmas[idx_enfants[u]]),to);
    		mutation_couche_normale(&filsf, &(sigmas[idx_enfants[u]]),to);

    	}

    	int id_mxv =0;
    	int mxv =0;
    	for (u = 0; u < SIZE  ; ++u){
    		if (mxv < scores[u]) id_mxv = u;
  			mxv = fmax(mxv,scores[u]);

    	}

    	free_plateau(gfin);
    	plateau tmp;
    	list_elementAt(ensemble_plt, id_mxv, &tmp);
    	gfin = copie_plateau(tmp);


    	/*Réinitialistation*/
    	
    	free_liste(ensemble_plt);
    	del_mains(ensemble_main);
    	
    	
    	free_plateau(g);
    	free_liste(listetmp);

    	free(idx_enfants);
    	free(idx_parents);
    	free(idx_parents_co);
    	

    	printf("gen : %d | ", t);
    	/*for (u = 0; u < SIZE; ++u){
    		printf("sc%d : %d ",u,scores[u]);

    	}*/
    	printf("%d", mxv);
    	printf(" |\n");
    	
    }

    save_population(M1, "pop1_sz48_s07_to2_g100.txt");
    save_population(M2, "pop1_sz48_s07_to2_g100.txt");
    save_population(Mf, "pop1_sz48_s07_to2_g100.txt");

    del_population(M1);
    del_population(M2);
    del_population(Mf);
    
    free(sigmas);

    print_plateau(gfin);
    free_plateau(gfin);

    int mx =0;
    for (u = 0; u < SIZE  ; ++u){
  		mx = fmax(mx,scores[u]);
    }
    free(scores);
	return mx;
}
