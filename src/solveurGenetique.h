/**
*\file solveur.h
*Ce fichier decrit le solveur : Honshu_Opt
*
*/

#ifndef __SOLVEURGENETIQUE_h__
#define __SOLVEURGENETIQUE_h__

#include <stdio.h>
#include <string.h>
#include <sys/types.h>

#include <unistd.h>
#include <time.h>
#include "solveurGenetique.h"

#include "Plateau.h"
#include "tuile.h"
#include "insertion.h"
#include "Liste_tuiles.h"

#include "loadAndSave.h"
#include "pile.h"
#include "jeu.h"
#include "compte_point.h"


int train(int argc, char *argv[]);

#endif