#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "Plateau.h"
#include "tuile.h"
#include "insertion.h"
#include "Liste_tuiles.h"
#include "loadAndSave.h"
#include "pile.h"
#include "jeu.h"
#include "compte_point.h"

#include "solveur_naif.h"

#include "solveur_bis.h"

int Honshu_Opt_bis(plateau g, int n, list T, list L, Stack pile)
{
    int best;
    int Tmp_Score;

    int i;
    int j;
    Stack tpile = pile;

    best = -1;

    if(list_is_empty(L)) {
        return points(g);
    }

    else
    {


        plateau current = copy_plateau(L->value);

        L = L->next;

    list temp_T = (T);

    list temp_L = (L);

    /*Stack temp_pile = tpile;*/
    /*Stack temp_pile_BIS = tpile;*/



        int rotation;
        for (rotation=0; rotation<4; rotation++)
        {


        for(i=0; i<n; i++)
        {
            for(j=0; j<n; j++)
            {


                    if (est_inserable(&g, current, i, j) == 0)
                    {

                        insert_plateau(&g, current, i, j);
                        temp_T = (T);
                        temp_L = (L);

                        /*temp_pile = tpile;*/

                        list_eADD(&temp_T, current);
                        quadruplet qua = create_quadruplet(current.values[0][0].numero, i, j, rotation);
                        push(&tpile, qua);

                        Tmp_Score = Honshu_Opt_bis(g, n, temp_T , temp_L, tpile);


                        best = max(Tmp_Score, best);

                        undo(&g, n, &temp_L, &tpile);


                    }

            }
        }


        rotation_gauche(&current);

        }


    }

    return best;

}
