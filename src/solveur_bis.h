/**
*\file solveur.h
*Ce fichier decrit le solveur : Honshu_Opt
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "Plateau.h"
#include "tuile.h"
#include "insertion.h"
#include "Liste_tuiles.h"
#include "loadAndSave.h"
#include "pile.h"
#include "jeu.h"
#include "compte_point.h"
#include "solveur_naif.h"



#ifndef __SOLVEUR_BIS_h__
#define __SOLVEUR_BIS_h__

int Honshu_Opt_bis(plateau g, int n, list T, list L, Stack pile);


#endif
