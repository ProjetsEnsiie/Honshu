#include "solveur_naif.h"

plateau copy_plateau(plateau p) {
  int n=p.width;
  int x=p.length;
  plateau m=create_plateau(x,n);
  int i,j;
  for (i=0;i<x;i++) {
    for (j=0;j<n;j++) {
      m.values[i][j]=p.values[i][j];
    }
  }
  return m;
}

char* terrains_recouverts_par (plateau p,plateau t,int x, int y) {
  int n,m,i,j;
  n=t.length;
  m=t.width;
  char* c=(char*)malloc(sizeof(char)*6);
  for (i=0;i<n;i++) {
    for (j=0;j<m;j++) {
      c[i*m+j]=p.values[x+i][y+j].type;
    }
  }
  return c;
}

int char_not_in_string(char c, char* t,int n) {
  int i;
  for ( i=0;i<n;i++) {
    if (t[i]==c) {
      free (t);
      return 0;
    }
  }
  free(t);
  return 1;
}

void frontiere(plateau p,int** ligne,int** colonne) {
  int n=p.length;
  int i,j;
  for (i =0;i<n;i++) {
    for (j=0;j<n;j++) {
      if (p.values[i][j].type !=' ') {
	if (ligne[i][0]>j) {
	  if (j<2) {ligne[i][0]=0;}
	  else {ligne[i][0]=j-2;}
	}
	if (ligne[i][1]<j) {
	  ligne[i][1]=j;
	}
	if (colonne[i][0]>i) {
	  if (i<2) {colonne[j][0]=0;}
	  else {colonne[j][0]=i-2;}
	}
	if (ligne[j][1]<i) {
	  ligne[j][1]=i;
	}
      }
    }
  }
}


void nb_terrains_tuiles(int* t,list l) {
  while (!list_is_empty(l)) {
    compte_nb_terrains(l->value, t);
    l=l->next;
  }
}

int** create_matrix_ligne(int n) {
  int i;
  int** M;
  M=(int**)(malloc(n*sizeof(int*)));
  for (i=0;i<n;i++) {
    M[i]=(int*)malloc(2*sizeof(int));
    M[i][0]=0;
    M[i][1]=n;
  }
  return (M);
}

void favorise_usine (plateau p,list l) {
  int i,j,k,n, bool,bool2,bool3;
  n=p.length;
  int** ligne=create_matrix_ligne (n);
  int** colonne=create_matrix_ligne (n);
  plateau* t;
  list c=l;
  
  while (!list_is_empty(c)) {
    bool2=0;
    bool=0;
    bool3=0;
    t=&(c->value);
    print_plateau(p);
    print_list(c);
    frontiere(p,ligne,colonne);
    
    for (i=0;i<n;i++) {
      for (j=ligne[i][0];j<ligne[i][1];j++) {
	for (k=0;k<4;k++) {

	  if (!est_inserable(&p,*t,i,j) ) {

	    if (char_not_in_string('U',terrains_recouverts_par(p,*t,i,j),6)
		&&  char_not_in_string('R',terrains_recouverts_par(p,*t,i,j),6)) {
	      bool=1;
	      bool2=1;
	      bool3=1;
	      c=c->next;
	      break;
	    }
	  }
	   
	  rotation_gauche(t);
	}
      
	if (bool==1)  {
	  break;
	}
      }
      if (bool3) {
	break;
      }
      
    }

    if (bool2==0) {

      for (i=0;i<n;i++) {
	for (j=ligne[i][0];j<ligne[i][1];j++) {
	  for (k=0;k<4;k++) {

	    if (!est_inserable(&p,*t,i,j)) {
	      bool=1;
	      bool2=1;
	      bool3=1;
	      c=c->next;
	      break;
	    }
	    rotation_gauche(t);
	  }

	  if (bool3) {
	    break;
	    }
	}

	if (bool==1)  {
	  break;
	}
      }
    }
     if (bool2==0) {
    printf("pas insérable\n");
    break;
  }

  }
 
  free_liste(l);
}


void favorise_foret (plateau p,list l) {
  int i,j,k,n, bool,bool2,bool3;
  n=p.width;
  int** ligne=create_matrix_ligne (n);
  int** colonne= create_matrix_ligne (n);
  plateau* t;
  list c=l;

  while (!list_is_empty(c)) {
    bool2=0;
    bool=0;
    bool3=0;
    t=&(c->value);
    frontiere(p,ligne,colonne);

    for (i=0;i<n;i++) {
      for (j=ligne[i][0];j<ligne[i][1];j++) {
	for (k=0;k<4;k++) {

	  if ( !est_inserable(&p,*t,i,j)) {

	    if (char_not_in_string('F',terrains_recouverts_par(p,*t,i,j),6)) {
	      insert_plateau(&p,*t,i,j);
	      bool=1;
	      bool2=1;
	      bool3=1;
	      c=c->next;
	      break;
	    }
	  }
	  
	   
	  rotation_gauche(t);
	}

	if (bool==1)  {
	  break;
	}
	
      }
      
      if (bool3) {
	break;
      }
      free_liste(l);
    }
  
    /*on ne peut pas ne pas recouvrir de forêts*/ 
    if (bool2==0) {
      for (i=0;i<n;i++) {
	for (j=ligne[i][0];j<ligne[i][1];j++) {
	  for (k=0;k<4;k++) {

	    if (!est_inserable(&p,l->value,i,j)) {
	      insert_plateau(&p,*t,i,j);
	      bool=1;
	      bool2=1;
	      c=c->next;
	      break;
	    }
	       
	    rotation_gauche(t);
	  }
	     
	}
	   
	if (bool==1)  {
	  break;
	}
	   
      }
    }
  }
     
  if (bool2==0) {
    printf("pas insérable");
  }
  free_liste(l);
}


void favorise_ville (plateau p,list l) {
  int i,j,k,n,x,y,max,bool2;
  n=p.width;
  int** ligne=create_matrix_ligne(n);
  int** colonne=create_matrix_ligne(n);
  frontiere(p,ligne,colonne);
  plateau* t,pt;
  list c;
  c=l;
  plateau cp;
  
  while (!list_is_empty(c)) {
    max=0;
    bool2=0;
    frontiere(p,ligne,colonne);
    t=&(c->value);
    cp=copy_plateau(p);
    x=0;
    y=0;
    pt=copy_plateau(c->value);

    for (i=0;i<n;i++) {
      for (j=ligne[i][0];j<ligne[i][1];j++) {
	for (k=0;k<4;k++) {

	  if (!est_inserable(&p,*t,i,j) ) {
	    insert_plateau(&cp,*t,i,j);
	      
	    if (plus_grand_village(cp)>max ) {
	      x=i;
	      y=j;
	      free_plateau(pt);
	      pt=copy_plateau(*t);
	      max=plus_grand_village(cp);
	     
	    }
	    bool2=1;
	    free_plateau(cp);
	    cp=copy_plateau(p);
	  }
	
	    rotation_gauche(t);
	  
	}
      }
    }
    c=c->next;
    insert_plateau(&p,pt,x,y);
  }
  
   if (!bool2) {
    printf("pas insérable");
  }
}

int solveur_naif (plateau p, list l) {
  int* t=calloc(5,sizeof(int));
  nb_terrains_tuiles(t,l);
  printf("%d %d %d\n",2*t[1],4*min(t[3],t[4]),t[0]);
  if ((2*t[1] > 4*min(t[3],t[4])) && (2*t[1] > t[0])) {
    favorise_foret(p,l);
  }
  else if (4*min(t[3],t[4])>2*t[1] && 4*min(t[3],t[4])>t[0]) {
    favorise_usine(p,l);
  }
  else {
    favorise_ville(p,l);
  }
  print_plateau(p);
  printf("%d\n",points(p));
  return(points(p));
  free_plateau(p);

}


      
  


  
