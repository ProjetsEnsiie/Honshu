/**
*\file tuile.h
*Ce fichier decrit les fonctions liées aux tuiles. Il contient 6 fonctions :
*- get_string_tuile(tuile) renvoie le string lié à la tuile
*- create_plateau(c, n) Créer la tuile numéro \a n à partir de la chaîne de caractères \a c n remplit les lignes de la matrice dans l'ordre
*- rand_terrain ()  Donne un terrain aléatoire
*- rand_tuile (int n) créer la tuile numéro \a n avec des terrains aléatoire
*- plateau rotation_gauche/droite (plateau tuile) renvoie la tuile avec une rotation gauche/droite
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "Plateau.h"
#include "tuile.h"
#include "insertion.h"
#include "Liste_tuiles.h"
#include "loadAndSave.h"
#include "pile.h"
#include "jeu.h"
#include "compte_point.h"

#ifndef _SOLVEUR_NAIF__h__
#define __SOLVEUR_NAIF_h__

plateau copy_plateau(plateau p);

char* terrains_recouverts_par (plateau p,plateau t,int x, int y);

int char_not_in_string(char c, char* t,int n);

void frontiere(plateau p,int** ligne,int** colonne);

void nb_terrains_tuiles(int* t,list l);

int** create_matrix_ligne(int n);

void favorise_usine (plateau p,list l);

void favorise_foret (plateau p,list l);

void favorise_ville (plateau p,list l);

int solveur_naif (plateau p, list l);

#endif
