#include "tuile.h"
#include "Liste_tuiles.h"


char* terrains_recouvert_par (plateau p,plateau t,int x, int y) {
  int n,m,i,j;
  n=t.length;
  m=t.width;
  char* c=(char*)malloc(sizeof(char)*6);
  for (i=0;i<n;i++) {
    for (j=0;j<m;j++) {
      c[i*m+j]=p.values[x+i][y+j].type;
    }
  }
  return c;
}

int char_not_in_string(char c, char* t,int n) {
  for (int i=0;i<n;i++) {
    if (t[i]==c) {
      free (t);
      return 0;
  }
    free (t);
  return 1;
  
}

void frontiere(plateau p,int** ligne,int** colonne) {
  int n=p.length;
  int i,j;
  for (i =0;i<n;i++) {
    for (j=0;j<n;j++) {
      if (p.values[i][j].type !=' ') {
	if (ligne[i][0]>j) {
	  if (j<2) {ligne[i][0]=0;}
	  else {ligne[i][0]=j-2;}
	}
	if (ligne[i][1]<j) {
	  ligne[i][1]=j;
	}
	if (colonne[i][0]>i) {
	  if (i<2) {colonne[j][0]=0;}
	  else {colonne[j][0]=i-2;}
	}
	if (ligne[j][1]<i) {
	  ligne[j][1]=i;
	}
      }
    }
  }
}

void nb_terrains_tuiles(int* t,list l) {
  while (!list_is_empty(l)) {
    compte_nb_terrains(l->value,int* t);
    l=l->next;
  }
}

 void create_matrix_ligne(int n) {
   int i;
   int** M;
   M=(int**)(malloc(n*sizeof(int*)));
   for (i=0;i<n;i++) {
     M[i]=(int*)malloc(2*sizeof(int));
     M[i][0]=0;
     M[i][1]=n;
   }
 }

 void favorise_usine (plateau p,list l) {
   int i,j,k,n, bool,bool2;
   n=p.width;
   int** ligne=create_matrix_ligne(n);
   int** colonne=create_matrix_ligne(n);
   frontiere(p,ligne,colonne);
   bool=0;
   bool2=0;
   while (!list_is_empty(l)) {
     bool2=0;
     bool=0;
     for (i=0;i<n;i++) {
       for (j=ligne[i][0];j<ligne[i][1];j++) {
	 for (k=0;k<4;k++) {
	   if (est_inserable(p,l->value,i,j) &&
	       char_not_in_string('U',terrains_recouverts_par(p,l->value,i,j),6)
	       &&  char_not_in_string('R',terrains_recouverts_par(p,l->value,i,j),6)) {
	     poser_tuile(p,l->value,i,j);
	     break;
	     bool=1;
	     bool2=1;
	   }
	   rotation_gauche(l->value);
	 }
       }
       if (bool==1)  {
	 break;
       }
     }
     if (bool2==0) {
       for (i=0;i<n;i++) {
	 for (j=ligne[i][0];j<ligne[i][1];j++) {
	   for (k=0;k<4;k++) {
	     if (est_inserable(p,l->value,i,j) {
		 poser_tuile(p,l->value,i,j);
		 break;
		 bool=1;
		 bool2=1;
	       }
	       rotation_gauche(l->value);
	       }
	   }
	   if (bool==1)  {
	     break;
	   }
	 }
       }
     }
     if (bool2==0) {
       printf("pas insérable");
       break;
     }
     l=l->next;
   }
 }

  void favorise_foret (plateau p,list l) {
   int i,j,k,n, bool,bool2;
   n=p.width;
   int** ligne=create_matrix_ligne(n);
   int** colonne=create_matrix_ligne(n);
   frontiere(p,ligne,colonne);
   bool=0;
   bool2=0;
   while (!list_is_empty(l)) {
     bool2=0;
     bool=0;
     for (i=0;i<n;i++) {
       for (j=ligne[i][0];j<ligne[i][1];j++) {
	 for (k=0;k<4;k++) {
	   if (est_inserable(p,l->value,i,j) &&
	       char_not_in_string('F',terrains_recouverts_par(p,l->value,i,j),6)) {
	     poser_tuile(p,l->value,i,j);
	     break;
	     bool=1;
	     bool2=1;
	   }
	   rotation_gauche(l->value);
	 }
       }
       if (bool==1)  {
	 break;
       }
     }
     if (bool2==0) {
       for (i=0;i<n;i++) {
	 for (j=ligne[i][0];j<ligne[i][1];j++) {
	   for (k=0;k<4;k++) {
	     if (est_inserable(p,l->value,i,j) {
		 poser_tuile(p,l->value,i,j);
		 break;
		 bool=1;
		 bool2=1;
	       }
	       rotation_gauche(l->value);
	       }
	   }
	   if (bool==1)  {
	     break;
	   }
	 }
       }
     }
     if (bool2==0) {
       printf("pas insérable");
       break;
     }
     l=l->next;
   }
 }
	  
  
  
  


  
