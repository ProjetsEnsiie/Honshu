#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include "tuile.h"
#include "Plateau.h"

#include "solveur_naif.h"









char* get_string_tuile(plateau tuile) {

  int n,m;
  n=tuile.length;
  m=tuile.width;
  char* t = malloc(sizeof(char)*n*m +1);
  int i,j;
  for (i=0;i<n;i++) {
    for (j=0;j<m;j++) {
      t[i*m+j]=tuile.values[i][j].type;
	   }
  }
  t[n*m ] = '\0';
  return t;
}



plateau create_tuile(char* c, int n) {
  plateau tuile=create_plateau(3,2);
  int i,j;
  for (i=0;i < 3;i++) {
    for (j=0; j<2; j++) {
      tuile.values[i][j].numero=n;
    }
  }
  tuile.values[0][0].type=c[0];
  tuile.values[0][1].type=c[1];
  tuile.values[1][0].type=c[2];
  tuile.values[1][1].type=c[3];
  tuile.values[2][0].type=c[4];
  tuile.values[2][1].type=c[5];
  return tuile;
}

char rand_terrain () {
  int n=rand()%6;
  if (0==n) {
    return 'V';
  }
  else if (1==n) {
    return 'L';
  }
  else if (2==n) {
    return 'F';
  }
  else if (3==n) {
    return 'R';
  }
  else if (3==4){
    return 'U';
  }
  else {
    return 'P';
  }
}

plateau rand_tuile (int n) {
  plateau tuile=create_plateau(3,2);
  int i,j;
  for (i=0;i < 3;i++) {
    for (j=0; j<2; j++) {
      tuile.values[i][j].type=rand_terrain();
      tuile.values[i][j].numero=n;
    }
  }
  return tuile;
}

plateau rotation_gauche_ancien (plateau tuile) {
  int i,j;
  int n=tuile.values[0][0].numero;
  if (tuile.length == 3) {
    plateau new_tuile=create_plateau(2,3);
    for (i=0;i<3;i++) {
      for (j=0; j<2; j++) {
	new_tuile.values[j][i].type=tuile.values[i][1-j].type;
	new_tuile.values[j][i].numero=n;
      }
    }
    /*free_plateau(tuile);*/
    return (new_tuile);
  }
  else {
    plateau new_tuile=create_plateau(3,2);
    for (i=0;i<2;i++) {
      for (j=0; j<3; j++) {
	new_tuile.values[j][i].type=tuile.values[i][2-j].type;
	new_tuile.values[j][i].numero=n;
      }
    }
    /*free_plateau(tuile);*/
    return (new_tuile);
  }
}

void rotation_gauche(plateau* tuile) {
  int i,j;
  int n=tuile->values[0][0].numero;
  if (tuile->length == 3) {
    plateau new_tuile=create_plateau(2,3);
    for (i=0;i<3;i++) {
      for (j=0; j<2; j++) {
	new_tuile.values[j][i].type=tuile->values[i][1-j].type;
	new_tuile.values[j][i].numero=n;
      }
    }
    free_plateau(*tuile);
    *tuile=new_tuile;
  }
  else {
    plateau new_tuile=create_plateau(3,2);
    for (i=0;i<2;i++) {
      for (j=0; j<3; j++) {
	new_tuile.values[j][i].type=tuile->values[i][2-j].type;
	new_tuile.values[j][i].numero=n;
      }
    }
    free_plateau(*tuile);
    *tuile=new_tuile;
  }
}

/*plateau rotation_droite (plateau tuile) {
  int i,j;
  int n=tuile.values[0][0].numero;
  if (tuile.length == 3) {
    plateau new_tuile=create_plateau(2,3);
    for (i=0;i<3;i++) {
      for (j=0; j<2; j++) {
	new_tuile.values[j][i].type=tuile.values[2-i][j].type;
	new_tuile.values[j][i].numero=n;
      }
    }
    free_plateau(tuile);
    return (new_tuile);
  }
  else {
    plateau new_tuile=create_plateau(3,2);
    for (i=0;i<2;i++) {
      for (j=0; j<3; j++) {
	new_tuile.values[j][i].type=tuile.values[1-i][j].type;
	new_tuile.values[j][i].numero=n;
      }
    }
    free_plateau(tuile);
    return (new_tuile);
  }
}*/

void rotation_droite (plateau* tuile) {
  int i,j;
  int n=tuile->values[0][0].numero;
  if (tuile->length == 3) {
    plateau new_tuile=create_plateau(2,3);
    for (i=0;i<3;i++) {
      for (j=0; j<2; j++) {
	new_tuile.values[j][i].type=tuile->values[2-i][j].type;
	new_tuile.values[j][i].numero=n;
      }
    }
    free_plateau(*tuile);
    *tuile=new_tuile;
  }
  else {
    plateau new_tuile=create_plateau(3,2);
    for (i=0;i<2;i++) {
      for (j=0; j<3; j++) {
	new_tuile.values[j][i].type=tuile->values[1-i][j].type;
	new_tuile.values[j][i].numero=n;
      }
    }
    free_plateau(*tuile);
    *tuile=new_tuile;
  }
}



int equal_tuile(plateau plat1, plateau plat2)
{
    int i;
    int j;

    if(plat1.length != plat2.length)
    {
        return 0;
    }
    else
    {
        if(plat1.length == 3)
        {
            for (i=0; i<3; i++)
            {
                for (j=0; j<2; j++)
                {
                    if(plat1.values[i][j].type != plat2.values[i][j].type)
                    {
                        return 0;
                    }
                }
            }
            return 1;
        }

        else
        {
            for (i=0; i<2; i++)
            {
                for (j=0; j<3; j++)
                {
                    if(plat1.values[i][j].type != plat2.values[i][j].type)
                    {
                        return 0;
                    }
                }
            }
            return 1;
        }
    }
}



