/**
*\file tuile.h
*Ce fichier decrit les fonctions liées aux tuiles. Il contient 6 fonctions :
*- get_string_tuile(tuile) renvoie le string lié à la tuile
*- create_plateau(c, n) Créer la tuile numéro \a n à partir de la chaîne de caractères \a c n remplit les lignes de la matrice dans l'ordre
*- rand_terrain ()  Donne un terrain aléatoire
*- rand_tuile (int n) créer la tuile numéro \a n avec des terrains aléatoire
*- plateau rotation_gauche/droite (plateau tuile) renvoie la tuile avec une rotation gauche/droite
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "Plateau.h"

#ifndef __TUILE_h__
#define __TUILE_h__

/**
* \brief Renvoie la chaîne de caractère liée à la tuile \a tuile
* \param tuile Une tuile
* \return La chaîne de caractère
*/
char* get_string_tuile(plateau tuile);

/**
* \brief Créer la tuile numéro \a n à partir de la chaîne de caractères \a c
* \param c Un string contenant les terrains à placer dans la tuile
* \param n Le numéro de la tuile
* \return La tuile associée à la chaîne c
*/
plateau create_tuile(char* c, int n);

/**
* \brief Donne un terrain aléatoire
* \return Un terrain
*/
char rand_terrain ();

/**
* \brief Créer la tuile numéro \a n avec des terrains aléatoire
* \param n Le numéro de la tuile
* \return Une tuile avec des terrains aléatoire
*/
plateau rand_tuile (int n);

/**
* \brief Renvoie la tuile avec une rotation gauche
* \param tuile Un pointeur vers la tuile sur laquelle on veut effectuer la rotation
*/
void rotation_gauche (plateau* tuile);

plateau rotation_gauche_ancien (plateau tuile);

/**
* \brief Renvoie la tuile avec une rotation droite
* \param tuile Un pointeur la tuile sur laquelle on veut effectuer la rotation
*/
void rotation_droite (plateau* tuile);

int equal_tuile(plateau plat1, plateau plat2);


#endif
