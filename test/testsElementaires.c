#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"
#include "../src/insertion.h"
#include "../src/Plateau.h"
#include "../src/tuile.h"
#include "../src/loadAndSave.h"

#include "../src/main.h"


void test_creation_plateau() {
    plateau test = create_plateau(5, 5);
    int i;
    int j;
    for (i=0; i<5; i++)
    {
        for (j=0; j<5; j++)
        {
            CU_ASSERT_EQUAL(lecture_case(test, i, j) == ' ', 1);

        }
    }
}

void test_creation_plateau_fichier() {
    plateau g;
    list liste = create_list();
    load_partie("tuiles.txt", "partie1.txt", &g, &liste);
    int i;
    int j;
    for (i=0; i<g.length; i++)
    {
        for (j=0; j<g.width; j++)
        {
            if (i==3 && j==3)
            {
                CU_ASSERT_EQUAL(lecture_case(g, i, j) == 'U', 1);
            }
            else if (i==3 && j==4)
            {
                CU_ASSERT_EQUAL(lecture_case(g, i, j) == 'F', 1);
            }
            else if (i==4 && j==3)
            {
                CU_ASSERT_EQUAL(lecture_case(g, i, j) == 'R', 1);
            }
            else if (i==4 && j==4)
            {
                CU_ASSERT_EQUAL(lecture_case(g, i, j) == 'P', 1);
            }
            else if (i==5 && j==3)
            {
                CU_ASSERT_EQUAL(lecture_case(g, i, j) == 'V', 1);
            }
            else if (i==5 && j==4)
            {
                CU_ASSERT_EQUAL(lecture_case(g, i, j) == 'V', 1);
            }
            else
            {
                CU_ASSERT_EQUAL(lecture_case(g, i, j) == ' ', 1);
            }

        }
    }
}

void test_tuile() {
    plateau tuile_test = create_tuile("LVULRV",0);
    CU_ASSERT_EQUAL(lecture_case(tuile_test,0,0) == 'L', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test,0,1) == 'V', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test,1,0) == 'U', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test,1,1) == 'L', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test,2,0) == 'R', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test,2,1) == 'V', 1);

}

void test_tuile_num() {
    plateau tuile_test = create_tuile("LVULRV",1);
    CU_ASSERT_EQUAL(lecture_case_num(tuile_test,0,0), 1);
    CU_ASSERT_EQUAL(lecture_case_num(tuile_test,0,1), 1);
    CU_ASSERT_EQUAL(lecture_case_num(tuile_test,1,0), 1);
    CU_ASSERT_EQUAL(lecture_case_num(tuile_test,1,1), 1);
    CU_ASSERT_EQUAL(lecture_case_num(tuile_test,2,0), 1);
    CU_ASSERT_EQUAL(lecture_case_num(tuile_test,2,1), 1);

}

void test_rotation_tuile() {
    plateau tuile_test2 = create_tuile("VVURRV",0);
    rotation_gauche(&tuile_test2);
    CU_ASSERT_EQUAL(lecture_case(tuile_test2,0,0) == 'V', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test2,1,0) == 'V', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test2,0,1) == 'R', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test2,1,1) == 'U', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test2,0,2) == 'V', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test2,1,2) == 'R', 1);

}

void test_rotation_tuile2() {
    plateau tuile_test5 = create_tuile("LVULRV",0);
    rotation_gauche(&tuile_test5);
    rotation_gauche(&tuile_test5);
    rotation_gauche(&tuile_test5);
    rotation_gauche(&tuile_test5);
    CU_ASSERT_EQUAL(lecture_case(tuile_test5,0,0) == 'L', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test5,0,1) == 'V', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test5,1,0) == 'U', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test5,1,1) == 'L', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test5,2,0) == 'R', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test5,2,1) == 'V', 1);

}

void test_rotation_tuile3() {
    plateau tuile_test = create_tuile("LVULRV",0);
    rotation_gauche(&tuile_test);
    rotation_droite(&tuile_test);
    CU_ASSERT_EQUAL(lecture_case(tuile_test,0,0) == 'L', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test,0,1) == 'V', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test,1,0) == 'U', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test,1,1) == 'L', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test,2,0) == 'R', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test,2,1) == 'V', 1);

}

void test_tuile_fichier() {
    plateau tuile_test = load_tuile_n("tuiles.txt", 0);
    CU_ASSERT_EQUAL(lecture_case(tuile_test,0,0) == 'U', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test,0,1) == 'F', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test,1,0) == 'R', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test,1,1) == 'P', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test,2,0) == 'V', 1);
    CU_ASSERT_EQUAL(lecture_case(tuile_test,2,1) == 'V', 1);

}

void test_tuile_num_fichier() {
    plateau tuile_test = load_tuile_n("tuiles.txt", 0);
    CU_ASSERT_EQUAL(lecture_case_num(tuile_test,0,0), 0);
    CU_ASSERT_EQUAL(lecture_case_num(tuile_test,0,1), 0);
    CU_ASSERT_EQUAL(lecture_case_num(tuile_test,1,0), 0);
    CU_ASSERT_EQUAL(lecture_case_num(tuile_test,1,1), 0);
    CU_ASSERT_EQUAL(lecture_case_num(tuile_test,2,0), 0);
    CU_ASSERT_EQUAL(lecture_case_num(tuile_test,2,1), 0);

}


int setup()  { return 0; }
int teardown() { return 0; }

int main() {
    CU_pSuite pSuite = NULL;
    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();

    pSuite = CU_add_suite("Projet INFO", setup, teardown);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if ((NULL == CU_add_test(pSuite, "test_creation_plateau", test_creation_plateau)) ||
     (NULL == CU_add_test(pSuite, "test_creation_plateau_fichier", test_creation_plateau_fichier)) ||
     (NULL == CU_add_test(pSuite, "test_tuile", test_tuile)) ||
     (NULL == CU_add_test(pSuite, "test_tuile_num", test_tuile_num)) ||
     (NULL == CU_add_test(pSuite, "test_rotation_tuile", test_rotation_tuile)) ||
     (NULL == CU_add_test(pSuite, "test_rotation_tuile2", test_rotation_tuile2)) ||
     (NULL == CU_add_test(pSuite, "test_rotation_tuile3", test_rotation_tuile3)) ||
     (NULL == CU_add_test(pSuite, "test_tuile_fichier", test_tuile_fichier)) ||
     (NULL == CU_add_test(pSuite, "test_tuile_num_fichier", test_tuile_num_fichier))

    ){
        CU_cleanup_registry();
        return CU_get_error();
    }

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    printf("\n");
    CU_basic_show_failures(CU_get_failure_list());

    CU_cleanup_registry();

    return CU_get_error();
}
