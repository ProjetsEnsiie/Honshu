#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"
#include "../src/insertion.h"
#include "../src/Plateau.h"
#include "../src/tuile.h"

#include "../src/main.h"


void test_ajout_tuile() {
    plateau test = create_plateau(5,5);
    plateau tuile_1 = create_tuile("RVUURV",0);
    plateau tuile_ajout = create_tuile("UVURRV",1);
    insert_plateau_start(&test,tuile_1);
    CU_ASSERT_EQUAL(insert_plateau(&test, tuile_ajout, 2, 2), 0);
}

void test_ajout_tuile_coin() {
    plateau test = create_plateau(5,5);
    plateau tuile_2 = create_tuile("RVUURV",0);
    plateau tuile_ajout = create_tuile("UVURRV",1);
    insert_plateau_start(&test,tuile_2);
    CU_ASSERT_EQUAL(insert_plateau(&test, tuile_ajout, 2, 2), 0);
}

void error_for_lake() {
    plateau test = create_plateau(5,5);
    plateau tuile_lac = create_tuile("LVULRV",0);
    plateau tuile_ajout = create_tuile("UVURRV",1);
    insert_plateau_start(&test,tuile_lac);
    CU_ASSERT_NOT_EQUAL(insert_plateau(&test, tuile_ajout, 2, 2), 0);
}

void error_for_lake_coin() {
    plateau test = create_plateau(5,5);
    plateau tuile_lac = create_tuile("LVULRV",0);
    plateau tuile_ajout = create_tuile("UVURRV",1);
    insert_plateau(&test,tuile_lac,1,1);
    CU_ASSERT_NOT_EQUAL(insert_plateau(&test, tuile_ajout, 2, 3), 0);
}

void error_for_length() {
    plateau test = create_plateau(5,5);
    plateau tuile_l = create_tuile("UVURRV",0);
    plateau tuile_ajout = create_tuile("UVURRV",1);
    insert_plateau_start(&test,tuile_l);
    CU_ASSERT_NOT_EQUAL(insert_plateau(&test, tuile_ajout, 3, 3), 0);
}

void error_for_length2() {
    plateau test = create_plateau(5,5);
    plateau tuile_l = create_tuile("VVURRV",0);
    plateau tuile_ajout = create_tuile("UVURRV",1);
    insert_plateau_start(&test,tuile_l);
    CU_ASSERT_NOT_EQUAL(insert_plateau(&test, tuile_ajout, 0, 4), 0);
}

void error_recouvrement() {
    plateau test = create_plateau(5,5);
    plateau tuile_r = create_tuile("VVURRV",0);
    plateau tuile_ajout = create_tuile("UVURRV",1);
    insert_plateau_start(&test,tuile_r);
    CU_ASSERT_NOT_EQUAL(insert_plateau(&test, tuile_ajout, 1, 1), 0);
}

void error_recouvrement2() {
    plateau test = create_plateau(5,5);
    plateau tuile_r = create_tuile("VVURRV",0);
    plateau tuile_rr = create_tuile("VVVRRV",1);
    plateau tuile_ajout = create_tuile("UVURRV",2);
    insert_plateau_start(&test,tuile_r);
    insert_plateau(&test,tuile_rr,2,1);
    CU_ASSERT_NOT_EQUAL(insert_plateau(&test, tuile_ajout, 0, 1), 0);
}

void error_insert() {
    plateau test = create_plateau(5,5);
    plateau tuile_i = create_tuile("VVURRV",0);
    plateau tuile_ajout = create_tuile("UVURRV",1);
    insert_plateau_start(&test,tuile_i);
    CU_ASSERT_NOT_EQUAL(insert_plateau(&test, tuile_ajout, 0, 3), 0);
}

void error_insert2() {
    plateau test = create_plateau(5,5);
    plateau tuile_ii = create_tuile("VVURRV",0);
    plateau tuile_ajout = create_tuile("UVURRV",1);
    insert_plateau_start(&test,tuile_ii);
    CU_ASSERT_NOT_EQUAL(insert_plateau(&test, tuile_ajout, 3, 2), 0);
}
void nb_voisins_1() {
    plateau test = create_plateau(5,5);
    plateau tuile_ii = create_tuile("VVVVRV",0);
    insert_plateau_start(&test,tuile_ii);
    printf("\n");
    print_plateau(test);
    printf("%s\n","Nombre de voisins a la case 2,1 : 5" );
    int k;
    int **voisins;
    voisins = (int **) malloc(sizeof(int *) * 100);
    for (k = 0; k < 100; k++) {
        voisins[k] = (int *) malloc(sizeof(int) * 100);
    }
    CU_ASSERT_EQUAL(village(test, 2,1, voisins, 0) , 5);
    
    free(voisins);
}
void nb_voisins_2() {
    plateau test = create_plateau(5,5);
    plateau tuile_i = create_tuile("VVVVRV",0);
    plateau tuile_ii = create_tuile("VVVVRV",0);
    insert_plateau_start(&test,tuile_i);
    insert_plateau(&test, tuile_ii, 2, 2);
    printf("\n");
    print_plateau(test);
    int k;
    int **voisins;
    voisins = (int **) malloc(sizeof(int *) * 25);
    for (k = 0; k < 25; k++) {
        voisins[k] = (int *) malloc(sizeof(int) * 25);
    }
    printf("%s\n","Nombre de voisins a la case 2,2 : 8" );
    CU_ASSERT_EQUAL(village(test, 2,2, voisins, 0) , 8);
    voisins = (int **) malloc(sizeof(int *) * 25);
    for (k = 0; k < 25; k++) {
        voisins[k] = (int *) malloc(sizeof(int) * 25);
    }
    printf("%s\n","Nombre de voisins a la case 3,1 : 0" );
    CU_ASSERT_EQUAL(village(test, 3,1, voisins, 0) , 0);

    free(voisins);
}
void nb_voisins_3() {
    plateau test = create_plateau(10,10);
    plateau tuile_i = create_tuile("VVVRVV",0);
    plateau tuile_ii = create_tuile("VLVVRV",0);
    plateau tuile_iii = create_tuile("VRVVRV",0);
    plateau tuile_iv = create_tuile("VRVVRV",0);
    insert_plateau_start(&test,tuile_i);
    insert_plateau(&test, tuile_ii, 2, 3);
    insert_plateau(&test, tuile_iii, 6, 5);
    insert_plateau(&test, tuile_iv, 2, 5);
    printf("\n");
    print_plateau(test);
    int k;
    int **voisins;
    voisins = (int **) malloc(sizeof(int *) * 100);
    for (k = 0; k < 100; k++) {
        voisins[k] = (int *) malloc(sizeof(int) * 100);
    }

    printf("%s\n","Nombre de voisins a la case 2,3 : 14" );
    CU_ASSERT_EQUAL(village(test, 2,3, voisins, 0) , 14);

    for (k = 0; k < 100; k++) {
        voisins[k] = (int *) malloc(sizeof(int) * 100);
    }
    printf("%s\n","Nombre de voisins a la case 8,6 : 14" );
    CU_ASSERT_EQUAL(village(test, 8,6, voisins, 0) , 14);

    printf("%s\n","plus grand village :14" );
    CU_ASSERT_EQUAL(plus_grand_village(test), 14);

    free(voisins);
}
void nb_voisins_4() {
    plateau test = create_plateau(10,10);
    plateau tuile_i = create_tuile("VVVRVV",0);
    plateau tuile_ii = create_tuile("VLVVRV",0);
    plateau tuile_iii = create_tuile("RVUVVV",0);
    plateau tuile_iv = create_tuile("VRVVRV",0);
    plateau tuile_v = create_tuile("RVUURV",0);
    insert_plateau_start(&test,tuile_i);
    insert_plateau(&test, tuile_ii, 2, 3);
    insert_plateau(&test, tuile_iii, 6, 5);
    insert_plateau(&test, tuile_iv, 5, 3);
    insert_plateau(&test, tuile_v, 3, 5);
    printf("\n");
    print_plateau(test);
    int k;
    int **voisins;
    voisins = (int **) malloc(sizeof(int *) * 100);
    for (k = 0; k < 100; k++) {
        voisins[k] = (int *) malloc(sizeof(int) * 100);
    }

    printf("%s\n","Nombre de voisins a la case 2,3 : 4" );
    CU_ASSERT_EQUAL(village(test, 2,3, voisins, 0) , 4);

    for (k = 0; k < 100; k++) {
        voisins[k] = (int *) malloc(sizeof(int) * 100);
    }

    printf("%s\n","Nombre de voisins a la case 5,6 : 5" );
    CU_ASSERT_EQUAL(village(test, 5,6, voisins, 0) , 5);

    for (k = 0; k < 100; k++) {
        voisins[k] = (int *) malloc(sizeof(int) * 100);
    }

    printf("%s\n","Nombre de voisins a la case 5,3 : 4" );
    CU_ASSERT_EQUAL(village(test, 5,3, voisins, 0) , 4);
    printf("%s\n","plus grand village : 5" );
    printf("%d\n",plus_grand_village(test));
    CU_ASSERT_EQUAL(plus_grand_village(test), 5);

    free(voisins);
}

int setup()  { return 0; }
int teardown() { return 0; }

int main() {
    CU_pSuite pSuite = NULL;
    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();

    pSuite = CU_add_suite("Projet INFO", setup, teardown);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if ((NULL == CU_add_test(pSuite, "error_for_lake", error_for_lake)) ||
     (NULL == CU_add_test(pSuite, "error_for_lake_coin", error_for_lake_coin)) ||
     (NULL == CU_add_test(pSuite, "error_for_length", error_for_length)) ||
     (NULL == CU_add_test(pSuite, "error_for_length2", error_for_length2)) ||
     (NULL == CU_add_test(pSuite, "test_ajout_tuile", test_ajout_tuile)) ||
     (NULL == CU_add_test(pSuite, "test_ajout_tuile_coin", test_ajout_tuile_coin)) ||
     (NULL == CU_add_test(pSuite, "error_recouvrement", error_recouvrement)) ||
     (NULL == CU_add_test(pSuite, "error_recouvrement2", error_recouvrement2)) ||
     (NULL == CU_add_test(pSuite, "error_insert", error_insert)) ||
     (NULL == CU_add_test(pSuite, "error_insert2", error_insert2)) ||
     (NULL == CU_add_test(pSuite, "nb_voisins_1", nb_voisins_1)) ||
     (NULL == CU_add_test(pSuite, "nb_voisins_2", nb_voisins_2)) ||
     (NULL == CU_add_test(pSuite, "nb_voisins_3", nb_voisins_3)) ||
     (NULL == CU_add_test(pSuite, "nb_voisins_3", nb_voisins_4))

    ) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    printf("\n");
    CU_basic_show_failures(CU_get_failure_list());

    CU_cleanup_registry();

    return CU_get_error();
}
